/********************************************************************************
** Form generated from reading UI file 'OrientedGaussian.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ORIENTEDGAUSSIAN_H
#define UI_ORIENTEDGAUSSIAN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "QVTKOpenGLNativeWidget.h"

QT_BEGIN_NAMESPACE

class Ui_OrientedGaussian
{
public:
    QAction *actionOpenFile;
    QAction *actionExit;
    QAction *actionPrint;
    QAction *actionHelp;
    QAction *actionSave;
    QAction *actionOpen;
    QAction *actionNew;
    QAction *actionClearAll;
    QAction *actionEllipse_scale;
    QAction *actionTensorSettings;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QToolBox *toolBox;
    QWidget *Images;
    QHBoxLayout *horizontalLayout_2;
    QTreeWidget *treeWidget;
    QWidget *Operations;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButtonSelect;
    QPushButton *pushButtonPickEllipses;
    QPushButton *pushButtonRemoveEllipses;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButtonGaussian;
    QSpinBox *spinBoxGaussIter;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *pushButtonTensor;
    QSpinBox *spinBoxTensorIter;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButtonLHE;
    QSpinBox *spinBoxLHEIter;
    QPushButton *pushButtonExportVTK;
    QPushButton *pushButtonExportPGM;
    QSpacerItem *verticalSpacer;
    QWidget *dockWidget;
    QTabWidget *tabWidget;
    QVTKOpenGLNativeWidget *GLWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEdit;

    void setupUi(QMainWindow *OrientedGaussian)
    {
        if (OrientedGaussian->objectName().isEmpty())
            OrientedGaussian->setObjectName(QString::fromUtf8("OrientedGaussian"));
        OrientedGaussian->resize(1251, 796);
        OrientedGaussian->setTabShape(QTabWidget::Rounded);
        actionOpenFile = new QAction(OrientedGaussian);
        actionOpenFile->setObjectName(QString::fromUtf8("actionOpenFile"));
        actionOpenFile->setEnabled(true);
        actionExit = new QAction(OrientedGaussian);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionPrint = new QAction(OrientedGaussian);
        actionPrint->setObjectName(QString::fromUtf8("actionPrint"));
        actionHelp = new QAction(OrientedGaussian);
        actionHelp->setObjectName(QString::fromUtf8("actionHelp"));
        actionSave = new QAction(OrientedGaussian);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionOpen = new QAction(OrientedGaussian);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionNew = new QAction(OrientedGaussian);
        actionNew->setObjectName(QString::fromUtf8("actionNew"));
        actionClearAll = new QAction(OrientedGaussian);
        actionClearAll->setObjectName(QString::fromUtf8("actionClearAll"));
        actionEllipse_scale = new QAction(OrientedGaussian);
        actionEllipse_scale->setObjectName(QString::fromUtf8("actionEllipse_scale"));
        actionTensorSettings = new QAction(OrientedGaussian);
        actionTensorSettings->setObjectName(QString::fromUtf8("actionTensorSettings"));
        centralwidget = new QWidget(OrientedGaussian);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        toolBox = new QToolBox(centralwidget);
        toolBox->setObjectName(QString::fromUtf8("toolBox"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(toolBox->sizePolicy().hasHeightForWidth());
        toolBox->setSizePolicy(sizePolicy);
        toolBox->setMinimumSize(QSize(160, 0));
        toolBox->setMaximumSize(QSize(100, 16777215));
        Images = new QWidget();
        Images->setObjectName(QString::fromUtf8("Images"));
        Images->setGeometry(QRect(0, 0, 160, 713));
        horizontalLayout_2 = new QHBoxLayout(Images);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        treeWidget = new QTreeWidget(Images);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        treeWidget->setHeaderItem(__qtreewidgetitem);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        sizePolicy.setHeightForWidth(treeWidget->sizePolicy().hasHeightForWidth());
        treeWidget->setSizePolicy(sizePolicy);
        treeWidget->setMinimumSize(QSize(140, 0));
        treeWidget->setMaximumSize(QSize(100, 16777215));
        treeWidget->header()->setVisible(false);

        horizontalLayout_2->addWidget(treeWidget);

        toolBox->addItem(Images, QString::fromUtf8("Images"));
        Operations = new QWidget();
        Operations->setObjectName(QString::fromUtf8("Operations"));
        Operations->setGeometry(QRect(0, 0, 160, 713));
        verticalLayout = new QVBoxLayout(Operations);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        pushButtonSelect = new QPushButton(Operations);
        pushButtonSelect->setObjectName(QString::fromUtf8("pushButtonSelect"));

        verticalLayout->addWidget(pushButtonSelect);

        pushButtonPickEllipses = new QPushButton(Operations);
        pushButtonPickEllipses->setObjectName(QString::fromUtf8("pushButtonPickEllipses"));
        pushButtonPickEllipses->setEnabled(true);
        pushButtonPickEllipses->setCheckable(true);

        verticalLayout->addWidget(pushButtonPickEllipses);

        pushButtonRemoveEllipses = new QPushButton(Operations);
        pushButtonRemoveEllipses->setObjectName(QString::fromUtf8("pushButtonRemoveEllipses"));

        verticalLayout->addWidget(pushButtonRemoveEllipses);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        pushButtonGaussian = new QPushButton(Operations);
        pushButtonGaussian->setObjectName(QString::fromUtf8("pushButtonGaussian"));

        horizontalLayout_3->addWidget(pushButtonGaussian);

        spinBoxGaussIter = new QSpinBox(Operations);
        spinBoxGaussIter->setObjectName(QString::fromUtf8("spinBoxGaussIter"));
        spinBoxGaussIter->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBoxGaussIter->setMinimum(1);
        spinBoxGaussIter->setMaximum(99999);

        horizontalLayout_3->addWidget(spinBoxGaussIter);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        pushButtonTensor = new QPushButton(Operations);
        pushButtonTensor->setObjectName(QString::fromUtf8("pushButtonTensor"));

        horizontalLayout_4->addWidget(pushButtonTensor);

        spinBoxTensorIter = new QSpinBox(Operations);
        spinBoxTensorIter->setObjectName(QString::fromUtf8("spinBoxTensorIter"));
        spinBoxTensorIter->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBoxTensorIter->setMinimum(1);
        spinBoxTensorIter->setMaximum(99999);

        horizontalLayout_4->addWidget(spinBoxTensorIter);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        pushButtonLHE = new QPushButton(Operations);
        pushButtonLHE->setObjectName(QString::fromUtf8("pushButtonLHE"));

        horizontalLayout_5->addWidget(pushButtonLHE);

        spinBoxLHEIter = new QSpinBox(Operations);
        spinBoxLHEIter->setObjectName(QString::fromUtf8("spinBoxLHEIter"));
        spinBoxLHEIter->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBoxLHEIter->setMinimum(1);
        spinBoxLHEIter->setMaximum(99999);

        horizontalLayout_5->addWidget(spinBoxLHEIter);


        verticalLayout->addLayout(horizontalLayout_5);

        pushButtonExportVTK = new QPushButton(Operations);
        pushButtonExportVTK->setObjectName(QString::fromUtf8("pushButtonExportVTK"));

        verticalLayout->addWidget(pushButtonExportVTK);

        pushButtonExportPGM = new QPushButton(Operations);
        pushButtonExportPGM->setObjectName(QString::fromUtf8("pushButtonExportPGM"));

        verticalLayout->addWidget(pushButtonExportPGM);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        toolBox->addItem(Operations, QString::fromUtf8("Operations"));

        horizontalLayout->addWidget(toolBox);

        dockWidget = new QWidget(centralwidget);
        dockWidget->setObjectName(QString::fromUtf8("dockWidget"));
        tabWidget = new QTabWidget(dockWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 1067, 757));
        tabWidget->setTabsClosable(true);
        GLWidget = new QVTKOpenGLNativeWidget(dockWidget);
        GLWidget->setObjectName(QString::fromUtf8("GLWidget"));
        GLWidget->setEnabled(true);
        GLWidget->setGeometry(QRect(20, 40, 1011, 801));

        horizontalLayout->addWidget(dockWidget);

        OrientedGaussian->setCentralWidget(centralwidget);
        menuBar = new QMenuBar(OrientedGaussian);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1251, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QString::fromUtf8("menuEdit"));
        OrientedGaussian->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionClearAll);
        menuEdit->addAction(actionEllipse_scale);
        menuEdit->addAction(actionTensorSettings);

        retranslateUi(OrientedGaussian);

        toolBox->setCurrentIndex(1);
        toolBox->layout()->setSpacing(1);


        QMetaObject::connectSlotsByName(OrientedGaussian);
    } // setupUi

    void retranslateUi(QMainWindow *OrientedGaussian)
    {
        OrientedGaussian->setWindowTitle(QCoreApplication::translate("OrientedGaussian", "Oriented Gaussian", nullptr));
        actionOpenFile->setText(QCoreApplication::translate("OrientedGaussian", "Open File...", nullptr));
        actionExit->setText(QCoreApplication::translate("OrientedGaussian", "Exit", nullptr));
        actionPrint->setText(QCoreApplication::translate("OrientedGaussian", "Print", nullptr));
        actionHelp->setText(QCoreApplication::translate("OrientedGaussian", "Help", nullptr));
        actionSave->setText(QCoreApplication::translate("OrientedGaussian", "Save", nullptr));
        actionOpen->setText(QCoreApplication::translate("OrientedGaussian", "Open", nullptr));
        actionNew->setText(QCoreApplication::translate("OrientedGaussian", "New", nullptr));
        actionClearAll->setText(QCoreApplication::translate("OrientedGaussian", "Clear All", nullptr));
        actionEllipse_scale->setText(QCoreApplication::translate("OrientedGaussian", "Ellipse scale", nullptr));
        actionTensorSettings->setText(QCoreApplication::translate("OrientedGaussian", "Tensor settings", nullptr));
        toolBox->setItemText(toolBox->indexOf(Images), QCoreApplication::translate("OrientedGaussian", "Images", nullptr));
        pushButtonSelect->setText(QCoreApplication::translate("OrientedGaussian", "Select", nullptr));
        pushButtonPickEllipses->setText(QCoreApplication::translate("OrientedGaussian", "Pick ellipses", nullptr));
        pushButtonRemoveEllipses->setText(QCoreApplication::translate("OrientedGaussian", "Remove ellipses", nullptr));
        pushButtonGaussian->setText(QCoreApplication::translate("OrientedGaussian", "Gaussian", nullptr));
        pushButtonTensor->setText(QCoreApplication::translate("OrientedGaussian", "Tensor", nullptr));
        pushButtonLHE->setText(QCoreApplication::translate("OrientedGaussian", "LHE", nullptr));
        pushButtonExportVTK->setText(QCoreApplication::translate("OrientedGaussian", "Export VTK", nullptr));
        pushButtonExportPGM->setText(QCoreApplication::translate("OrientedGaussian", "Export PGM", nullptr));
        toolBox->setItemText(toolBox->indexOf(Operations), QCoreApplication::translate("OrientedGaussian", "Operations", nullptr));
        menuFile->setTitle(QCoreApplication::translate("OrientedGaussian", "File", nullptr));
        menuEdit->setTitle(QCoreApplication::translate("OrientedGaussian", "Edit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class OrientedGaussian: public Ui_OrientedGaussian {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ORIENTEDGAUSSIAN_H
