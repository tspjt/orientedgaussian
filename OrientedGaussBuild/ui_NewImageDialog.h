/********************************************************************************
** Form generated from reading UI file 'NewImageDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWIMAGEDIALOG_H
#define UI_NEWIMAGEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_NewImageDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *lineEditImageName;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButtonOK;
    QPushButton *pushButtonCancel;

    void setupUi(QDialog *NewImageDialog)
    {
        if (NewImageDialog->objectName().isEmpty())
            NewImageDialog->setObjectName(QString::fromUtf8("NewImageDialog"));
        NewImageDialog->resize(272, 141);
        verticalLayout = new QVBoxLayout(NewImageDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(NewImageDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(NewImageDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        lineEditImageName = new QLineEdit(NewImageDialog);
        lineEditImageName->setObjectName(QString::fromUtf8("lineEditImageName"));

        horizontalLayout_2->addWidget(lineEditImageName);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButtonOK = new QPushButton(NewImageDialog);
        pushButtonOK->setObjectName(QString::fromUtf8("pushButtonOK"));

        horizontalLayout->addWidget(pushButtonOK);

        pushButtonCancel = new QPushButton(NewImageDialog);
        pushButtonCancel->setObjectName(QString::fromUtf8("pushButtonCancel"));

        horizontalLayout->addWidget(pushButtonCancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(NewImageDialog);

        QMetaObject::connectSlotsByName(NewImageDialog);
    } // setupUi

    void retranslateUi(QDialog *NewImageDialog)
    {
        NewImageDialog->setWindowTitle(QCoreApplication::translate("NewImageDialog", "Create new image", nullptr));
        label->setText(QCoreApplication::translate("NewImageDialog", "Create new image from selection?", nullptr));
        label_2->setText(QCoreApplication::translate("NewImageDialog", "Image name:", nullptr));
        lineEditImageName->setText(QCoreApplication::translate("NewImageDialog", "New Image", nullptr));
        pushButtonOK->setText(QCoreApplication::translate("NewImageDialog", "OK", nullptr));
        pushButtonCancel->setText(QCoreApplication::translate("NewImageDialog", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class NewImageDialog: public Ui_NewImageDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWIMAGEDIALOG_H
