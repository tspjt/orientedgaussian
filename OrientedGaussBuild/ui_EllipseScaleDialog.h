/********************************************************************************
** Form generated from reading UI file 'EllipseScaleDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ELLIPSESCALEDIALOG_H
#define UI_ELLIPSESCALEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_EllipseScaleDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QSlider *horizontalSlider;
    QDoubleSpinBox *doubleSpinBox;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButtonOK;
    QPushButton *pushButtonCancel;

    void setupUi(QDialog *EllipseScaleDialog)
    {
        if (EllipseScaleDialog->objectName().isEmpty())
            EllipseScaleDialog->setObjectName(QString::fromUtf8("EllipseScaleDialog"));
        EllipseScaleDialog->resize(308, 108);
        verticalLayout = new QVBoxLayout(EllipseScaleDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(EllipseScaleDialog);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        horizontalSlider = new QSlider(EllipseScaleDialog);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setMinimum(1);
        horizontalSlider->setMaximum(100);
        horizontalSlider->setValue(2);
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(horizontalSlider);

        doubleSpinBox = new QDoubleSpinBox(EllipseScaleDialog);
        doubleSpinBox->setObjectName(QString::fromUtf8("doubleSpinBox"));
        doubleSpinBox->setAlignment(Qt::AlignCenter);
        doubleSpinBox->setDecimals(1);
        doubleSpinBox->setMinimum(0.500000000000000);
        doubleSpinBox->setMaximum(50.000000000000000);
        doubleSpinBox->setSingleStep(0.500000000000000);
        doubleSpinBox->setValue(1.000000000000000);

        horizontalLayout_2->addWidget(doubleSpinBox);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButtonOK = new QPushButton(EllipseScaleDialog);
        pushButtonOK->setObjectName(QString::fromUtf8("pushButtonOK"));

        horizontalLayout->addWidget(pushButtonOK);

        pushButtonCancel = new QPushButton(EllipseScaleDialog);
        pushButtonCancel->setObjectName(QString::fromUtf8("pushButtonCancel"));

        horizontalLayout->addWidget(pushButtonCancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(EllipseScaleDialog);

        QMetaObject::connectSlotsByName(EllipseScaleDialog);
    } // setupUi

    void retranslateUi(QDialog *EllipseScaleDialog)
    {
        EllipseScaleDialog->setWindowTitle(QCoreApplication::translate("EllipseScaleDialog", "Set ellipse scale", nullptr));
        label->setText(QCoreApplication::translate("EllipseScaleDialog", "Scale:", nullptr));
        pushButtonOK->setText(QCoreApplication::translate("EllipseScaleDialog", "OK", nullptr));
        pushButtonCancel->setText(QCoreApplication::translate("EllipseScaleDialog", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class EllipseScaleDialog: public Ui_EllipseScaleDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ELLIPSESCALEDIALOG_H
