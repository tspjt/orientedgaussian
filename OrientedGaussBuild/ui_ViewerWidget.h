/********************************************************************************
** Form generated from reading UI file 'ViewerWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEWERWIDGET_H
#define UI_VIEWERWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ViewerWidget
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *Original;
    QHBoxLayout *horizontalLayout;
    QWidget *Result;
    QHBoxLayout *horizontalLayout_2;
    QWidget *Comparison;
    QHBoxLayout *horizontalLayout_3;

    void setupUi(QWidget *ViewerWidget)
    {
        if (ViewerWidget->objectName().isEmpty())
            ViewerWidget->setObjectName(QString::fromUtf8("ViewerWidget"));
        ViewerWidget->resize(535, 440);
        ViewerWidget->setAcceptDrops(true);
        verticalLayout = new QVBoxLayout(ViewerWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(ViewerWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setEnabled(true);
        tabWidget->setTabPosition(QTabWidget::East);
        Original = new QWidget();
        Original->setObjectName(QString::fromUtf8("Original"));
        horizontalLayout = new QHBoxLayout(Original);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tabWidget->addTab(Original, QString());
        Result = new QWidget();
        Result->setObjectName(QString::fromUtf8("Result"));
        horizontalLayout_2 = new QHBoxLayout(Result);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        tabWidget->addTab(Result, QString());
        Comparison = new QWidget();
        Comparison->setObjectName(QString::fromUtf8("Comparison"));
        horizontalLayout_3 = new QHBoxLayout(Comparison);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        tabWidget->addTab(Comparison, QString());

        verticalLayout->addWidget(tabWidget);


        retranslateUi(ViewerWidget);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ViewerWidget);
    } // setupUi

    void retranslateUi(QWidget *ViewerWidget)
    {
        ViewerWidget->setWindowTitle(QCoreApplication::translate("ViewerWidget", "Viewer widget", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Original), QCoreApplication::translate("ViewerWidget", "Original", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Result), QCoreApplication::translate("ViewerWidget", "Result", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Comparison), QCoreApplication::translate("ViewerWidget", "Comparison", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ViewerWidget: public Ui_ViewerWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEWERWIDGET_H
