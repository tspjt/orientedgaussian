/********************************************************************************
** Form generated from reading UI file 'TensorSettingsDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TENSORSETTINGSDIALOG_H
#define UI_TENSORSETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_TensorSettingsDialog
{
public:
    QVBoxLayout *verticalLayout_9;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_6;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QDoubleSpinBox *doubleSpinBoxOmegaOriginal;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QDoubleSpinBox *doubleSpinBoxTimeStepOriginal;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QSpinBox *spinBoxNumOfStepsOriginal;
    QGroupBox *groupBox_6;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_5;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_11;
    QDoubleSpinBox *doubleSpinBoxTimeStep;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_12;
    QSpinBox *spinBoxNumOfSteps;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QDoubleSpinBox *doubleSpinBoxOmegaA;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_7;
    QDoubleSpinBox *doubleSpinBoxOmegaB;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_10;
    QDoubleSpinBox *doubleSpinBoxOmegaC;
    QHBoxLayout *horizontalLayout_17;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_13;
    QDoubleSpinBox *doubleSpinBoxCParam;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_14;
    QDoubleSpinBox *doubleSpinBoxLambdaParam;
    QHBoxLayout *horizontalLayout_18;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButtonOK;

    void setupUi(QDialog *TensorSettingsDialog)
    {
        if (TensorSettingsDialog->objectName().isEmpty())
            TensorSettingsDialog->setObjectName(QString::fromUtf8("TensorSettingsDialog"));
        TensorSettingsDialog->resize(582, 253);
        verticalLayout_9 = new QVBoxLayout(TensorSettingsDialog);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        groupBox = new QGroupBox(TensorSettingsDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        horizontalLayout_6 = new QHBoxLayout(groupBox);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout_4 = new QVBoxLayout(groupBox_2);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        doubleSpinBoxOmegaOriginal = new QDoubleSpinBox(groupBox_2);
        doubleSpinBoxOmegaOriginal->setObjectName(QString::fromUtf8("doubleSpinBoxOmegaOriginal"));
        doubleSpinBoxOmegaOriginal->setDecimals(5);
        doubleSpinBoxOmegaOriginal->setMinimum(-1000.000000000000000);
        doubleSpinBoxOmegaOriginal->setMaximum(1000.000000000000000);
        doubleSpinBoxOmegaOriginal->setSingleStep(0.100000000000000);
        doubleSpinBoxOmegaOriginal->setValue(1.000000000000000);

        horizontalLayout->addWidget(doubleSpinBoxOmegaOriginal);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        doubleSpinBoxTimeStepOriginal = new QDoubleSpinBox(groupBox_2);
        doubleSpinBoxTimeStepOriginal->setObjectName(QString::fromUtf8("doubleSpinBoxTimeStepOriginal"));
        doubleSpinBoxTimeStepOriginal->setDecimals(3);
        doubleSpinBoxTimeStepOriginal->setMinimum(0.001000000000000);
        doubleSpinBoxTimeStepOriginal->setMaximum(1000.000000000000000);
        doubleSpinBoxTimeStepOriginal->setSingleStep(0.100000000000000);
        doubleSpinBoxTimeStepOriginal->setValue(0.250000000000000);

        horizontalLayout_2->addWidget(doubleSpinBoxTimeStepOriginal);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_3->addWidget(label_3);

        spinBoxNumOfStepsOriginal = new QSpinBox(groupBox_2);
        spinBoxNumOfStepsOriginal->setObjectName(QString::fromUtf8("spinBoxNumOfStepsOriginal"));
        spinBoxNumOfStepsOriginal->setMinimum(1);
        spinBoxNumOfStepsOriginal->setMaximum(9999);
        spinBoxNumOfStepsOriginal->setValue(3);

        horizontalLayout_3->addWidget(spinBoxNumOfStepsOriginal);


        verticalLayout->addLayout(horizontalLayout_3);


        verticalLayout_4->addLayout(verticalLayout);


        horizontalLayout_6->addWidget(groupBox_2);

        groupBox_6 = new QGroupBox(groupBox);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        verticalLayout_2 = new QVBoxLayout(groupBox_6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_11 = new QLabel(groupBox_6);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_11->addWidget(label_11);

        doubleSpinBoxTimeStep = new QDoubleSpinBox(groupBox_6);
        doubleSpinBoxTimeStep->setObjectName(QString::fromUtf8("doubleSpinBoxTimeStep"));
        doubleSpinBoxTimeStep->setDecimals(3);
        doubleSpinBoxTimeStep->setMinimum(0.001000000000000);
        doubleSpinBoxTimeStep->setMaximum(1000.000000000000000);
        doubleSpinBoxTimeStep->setSingleStep(0.100000000000000);
        doubleSpinBoxTimeStep->setValue(0.250000000000000);

        horizontalLayout_11->addWidget(doubleSpinBoxTimeStep);


        horizontalLayout_5->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_12 = new QLabel(groupBox_6);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        horizontalLayout_12->addWidget(label_12);

        spinBoxNumOfSteps = new QSpinBox(groupBox_6);
        spinBoxNumOfSteps->setObjectName(QString::fromUtf8("spinBoxNumOfSteps"));
        spinBoxNumOfSteps->setMinimum(1);
        spinBoxNumOfSteps->setMaximum(9999);
        spinBoxNumOfSteps->setValue(3);

        horizontalLayout_12->addWidget(spinBoxNumOfSteps);


        horizontalLayout_5->addLayout(horizontalLayout_12);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(groupBox_6);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_4->addWidget(label_4);

        doubleSpinBoxOmegaA = new QDoubleSpinBox(groupBox_6);
        doubleSpinBoxOmegaA->setObjectName(QString::fromUtf8("doubleSpinBoxOmegaA"));
        doubleSpinBoxOmegaA->setDecimals(5);
        doubleSpinBoxOmegaA->setMinimum(-1000.000000000000000);
        doubleSpinBoxOmegaA->setMaximum(1000.000000000000000);
        doubleSpinBoxOmegaA->setSingleStep(0.100000000000000);
        doubleSpinBoxOmegaA->setValue(1.000000000000000);

        horizontalLayout_4->addWidget(doubleSpinBoxOmegaA);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_7 = new QLabel(groupBox_6);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_7->addWidget(label_7);

        doubleSpinBoxOmegaB = new QDoubleSpinBox(groupBox_6);
        doubleSpinBoxOmegaB->setObjectName(QString::fromUtf8("doubleSpinBoxOmegaB"));
        doubleSpinBoxOmegaB->setDecimals(5);
        doubleSpinBoxOmegaB->setMinimum(-1000.000000000000000);
        doubleSpinBoxOmegaB->setMaximum(1000.000000000000000);
        doubleSpinBoxOmegaB->setSingleStep(0.100000000000000);
        doubleSpinBoxOmegaB->setValue(1.000000000000000);

        horizontalLayout_7->addWidget(doubleSpinBoxOmegaB);


        verticalLayout_2->addLayout(horizontalLayout_7);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_10 = new QLabel(groupBox_6);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_10->addWidget(label_10);

        doubleSpinBoxOmegaC = new QDoubleSpinBox(groupBox_6);
        doubleSpinBoxOmegaC->setObjectName(QString::fromUtf8("doubleSpinBoxOmegaC"));
        doubleSpinBoxOmegaC->setDecimals(5);
        doubleSpinBoxOmegaC->setMinimum(-1000.000000000000000);
        doubleSpinBoxOmegaC->setMaximum(1000.000000000000000);
        doubleSpinBoxOmegaC->setSingleStep(0.100000000000000);
        doubleSpinBoxOmegaC->setValue(1.000000000000000);

        horizontalLayout_10->addWidget(doubleSpinBoxOmegaC);


        verticalLayout_2->addLayout(horizontalLayout_10);


        horizontalLayout_6->addWidget(groupBox_6);


        verticalLayout_9->addWidget(groupBox);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_13 = new QLabel(TensorSettingsDialog);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout_16->addWidget(label_13);

        doubleSpinBoxCParam = new QDoubleSpinBox(TensorSettingsDialog);
        doubleSpinBoxCParam->setObjectName(QString::fromUtf8("doubleSpinBoxCParam"));
        doubleSpinBoxCParam->setMinimum(-1000.000000000000000);
        doubleSpinBoxCParam->setMaximum(1000.000000000000000);
        doubleSpinBoxCParam->setValue(0.500000000000000);

        horizontalLayout_16->addWidget(doubleSpinBoxCParam);


        horizontalLayout_17->addLayout(horizontalLayout_16);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        label_14 = new QLabel(TensorSettingsDialog);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_15->addWidget(label_14);

        doubleSpinBoxLambdaParam = new QDoubleSpinBox(TensorSettingsDialog);
        doubleSpinBoxLambdaParam->setObjectName(QString::fromUtf8("doubleSpinBoxLambdaParam"));
        doubleSpinBoxLambdaParam->setDecimals(4);
        doubleSpinBoxLambdaParam->setMinimum(0.000000000000000);
        doubleSpinBoxLambdaParam->setMaximum(1000.000000000000000);
        doubleSpinBoxLambdaParam->setValue(0.200000000000000);

        horizontalLayout_15->addWidget(doubleSpinBoxLambdaParam);


        horizontalLayout_17->addLayout(horizontalLayout_15);


        verticalLayout_9->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_18->addItem(horizontalSpacer);

        pushButtonOK = new QPushButton(TensorSettingsDialog);
        pushButtonOK->setObjectName(QString::fromUtf8("pushButtonOK"));

        horizontalLayout_18->addWidget(pushButtonOK);


        verticalLayout_9->addLayout(horizontalLayout_18);


        retranslateUi(TensorSettingsDialog);

        QMetaObject::connectSlotsByName(TensorSettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *TensorSettingsDialog)
    {
        TensorSettingsDialog->setWindowTitle(QCoreApplication::translate("TensorSettingsDialog", "Tensor method settings", nullptr));
        groupBox->setTitle(QCoreApplication::translate("TensorSettingsDialog", "LHE coef params", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("TensorSettingsDialog", "Original", nullptr));
        label->setText(QCoreApplication::translate("TensorSettingsDialog", "Omega:", nullptr));
        label_2->setText(QCoreApplication::translate("TensorSettingsDialog", "Timestep:", nullptr));
        label_3->setText(QCoreApplication::translate("TensorSettingsDialog", "Number of steps:", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("TensorSettingsDialog", "Coef settings", nullptr));
        label_11->setText(QCoreApplication::translate("TensorSettingsDialog", "Timestep:", nullptr));
        label_12->setText(QCoreApplication::translate("TensorSettingsDialog", "Number of steps:", nullptr));
        label_4->setText(QCoreApplication::translate("TensorSettingsDialog", "Omega A:", nullptr));
        label_7->setText(QCoreApplication::translate("TensorSettingsDialog", "Omega B:", nullptr));
        label_10->setText(QCoreApplication::translate("TensorSettingsDialog", "Omega C:", nullptr));
        label_13->setText(QCoreApplication::translate("TensorSettingsDialog", "C parameter:", nullptr));
        label_14->setText(QCoreApplication::translate("TensorSettingsDialog", "Lambda parameter:", nullptr));
        pushButtonOK->setText(QCoreApplication::translate("TensorSettingsDialog", "OK", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TensorSettingsDialog: public Ui_TensorSettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TENSORSETTINGSDIALOG_H
