/****************************************************************************
** Meta object code from reading C++ file 'OrientedGaussian.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "OrientedGaussian.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'OrientedGaussian.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_OrientedGaussian_t {
    QByteArrayData data[31];
    char stringdata0[624];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_OrientedGaussian_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_OrientedGaussian_t qt_meta_stringdata_OrientedGaussian = {
    {
QT_MOC_LITERAL(0, 0, 16), // "OrientedGaussian"
QT_MOC_LITERAL(1, 17, 8), // "slotExit"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 12), // "CreateNewTab"
QT_MOC_LITERAL(4, 40, 12), // "NewImageData"
QT_MOC_LITERAL(5, 53, 9), // "imageData"
QT_MOC_LITERAL(6, 63, 15), // "DisableCreation"
QT_MOC_LITERAL(7, 79, 14), // "EnableCreation"
QT_MOC_LITERAL(8, 94, 23), // "on_actionOpen_triggered"
QT_MOC_LITERAL(9, 118, 27), // "on_actionClearAll_triggered"
QT_MOC_LITERAL(10, 146, 30), // "on_tabWidget_tabCloseRequested"
QT_MOC_LITERAL(11, 177, 5), // "tabId"
QT_MOC_LITERAL(12, 183, 27), // "on_tabWidget_currentChanged"
QT_MOC_LITERAL(13, 211, 5), // "index"
QT_MOC_LITERAL(14, 217, 17), // "SetActiveTreeItem"
QT_MOC_LITERAL(15, 235, 24), // "OnViewerWidgetTabChanged"
QT_MOC_LITERAL(16, 260, 25), // "on_treeWidget_itemClicked"
QT_MOC_LITERAL(17, 286, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(18, 303, 4), // "item"
QT_MOC_LITERAL(19, 308, 3), // "col"
QT_MOC_LITERAL(20, 312, 27), // "on_pushButtonSelect_pressed"
QT_MOC_LITERAL(21, 340, 29), // "on_pushButtonGaussian_pressed"
QT_MOC_LITERAL(22, 370, 33), // "on_pushButtonPickEllipses_tog..."
QT_MOC_LITERAL(23, 404, 1), // "b"
QT_MOC_LITERAL(24, 406, 35), // "on_pushButtonRemoveEllipses_p..."
QT_MOC_LITERAL(25, 442, 32), // "on_actionEllipse_scale_triggered"
QT_MOC_LITERAL(26, 475, 33), // "on_actionTensorSettings_trigg..."
QT_MOC_LITERAL(27, 509, 27), // "on_pushButtonTensor_pressed"
QT_MOC_LITERAL(28, 537, 24), // "on_pushButtonLHE_pressed"
QT_MOC_LITERAL(29, 562, 30), // "on_pushButtonExportVTK_pressed"
QT_MOC_LITERAL(30, 593, 30) // "on_pushButtonExportPGM_pressed"

    },
    "OrientedGaussian\0slotExit\0\0CreateNewTab\0"
    "NewImageData\0imageData\0DisableCreation\0"
    "EnableCreation\0on_actionOpen_triggered\0"
    "on_actionClearAll_triggered\0"
    "on_tabWidget_tabCloseRequested\0tabId\0"
    "on_tabWidget_currentChanged\0index\0"
    "SetActiveTreeItem\0OnViewerWidgetTabChanged\0"
    "on_treeWidget_itemClicked\0QTreeWidgetItem*\0"
    "item\0col\0on_pushButtonSelect_pressed\0"
    "on_pushButtonGaussian_pressed\0"
    "on_pushButtonPickEllipses_toggled\0b\0"
    "on_pushButtonRemoveEllipses_pressed\0"
    "on_actionEllipse_scale_triggered\0"
    "on_actionTensorSettings_triggered\0"
    "on_pushButtonTensor_pressed\0"
    "on_pushButtonLHE_pressed\0"
    "on_pushButtonExportVTK_pressed\0"
    "on_pushButtonExportPGM_pressed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_OrientedGaussian[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  119,    2, 0x0a /* Public */,
       3,    1,  120,    2, 0x0a /* Public */,
       6,    0,  123,    2, 0x0a /* Public */,
       7,    0,  124,    2, 0x0a /* Public */,
       8,    0,  125,    2, 0x08 /* Private */,
       9,    0,  126,    2, 0x08 /* Private */,
      10,    1,  127,    2, 0x08 /* Private */,
      12,    1,  130,    2, 0x08 /* Private */,
      14,    1,  133,    2, 0x08 /* Private */,
      15,    1,  136,    2, 0x08 /* Private */,
      16,    2,  139,    2, 0x08 /* Private */,
      20,    0,  144,    2, 0x08 /* Private */,
      21,    0,  145,    2, 0x08 /* Private */,
      22,    1,  146,    2, 0x08 /* Private */,
      24,    0,  149,    2, 0x08 /* Private */,
      25,    0,  150,    2, 0x08 /* Private */,
      26,    0,  151,    2, 0x08 /* Private */,
      27,    0,  152,    2, 0x08 /* Private */,
      28,    0,  153,    2, 0x08 /* Private */,
      29,    0,  154,    2, 0x08 /* Private */,
      30,    0,  155,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, 0x80000000 | 17, QMetaType::Int,   18,   19,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void OrientedGaussian::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<OrientedGaussian *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->slotExit(); break;
        case 1: _t->CreateNewTab((*reinterpret_cast< NewImageData(*)>(_a[1]))); break;
        case 2: _t->DisableCreation(); break;
        case 3: _t->EnableCreation(); break;
        case 4: _t->on_actionOpen_triggered(); break;
        case 5: _t->on_actionClearAll_triggered(); break;
        case 6: _t->on_tabWidget_tabCloseRequested((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_tabWidget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->SetActiveTreeItem((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->OnViewerWidgetTabChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_treeWidget_itemClicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 11: _t->on_pushButtonSelect_pressed(); break;
        case 12: _t->on_pushButtonGaussian_pressed(); break;
        case 13: _t->on_pushButtonPickEllipses_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->on_pushButtonRemoveEllipses_pressed(); break;
        case 15: _t->on_actionEllipse_scale_triggered(); break;
        case 16: _t->on_actionTensorSettings_triggered(); break;
        case 17: _t->on_pushButtonTensor_pressed(); break;
        case 18: _t->on_pushButtonLHE_pressed(); break;
        case 19: _t->on_pushButtonExportVTK_pressed(); break;
        case 20: _t->on_pushButtonExportPGM_pressed(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject OrientedGaussian::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_OrientedGaussian.data,
    qt_meta_data_OrientedGaussian,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *OrientedGaussian::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *OrientedGaussian::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_OrientedGaussian.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int OrientedGaussian::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 21;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
