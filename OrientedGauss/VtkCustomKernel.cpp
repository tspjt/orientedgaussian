#include "VtkCustomKernel.h"

vtkStandardNewMacro(VtkCustomKernel);

VtkCustomKernel::VtkCustomKernel()
{

}

VtkCustomKernel::~VtkCustomKernel()
{

}

void VtkCustomKernel::SetCustomKernel(const double* kernel, int x, int y)
{
	SetKernel(kernel, x, y, 1);
}