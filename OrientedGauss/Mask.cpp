#include "Mask.h"

Mask::Mask()
{
	m_radius = 2;
}

Mask::~Mask()
{

}

Mask::Mask(const Mask& mask)
{
	m_radius = mask.m_radius;
	m_maskVals = mask.m_maskVals;
	
	SetupRowPtrs();
}

Mask::Mask(Mask&& mask)
{
	m_radius = mask.m_radius;
	m_maskVals = std::move(mask.m_maskVals);
	m_rowPtrs = std::move(mask.m_rowPtrs);
}

Mask& Mask::operator=(const Mask& mask)
{
	m_radius = mask.m_radius;
	m_maskVals = mask.m_maskVals;

	SetupRowPtrs();

	return *this;
}

Mask& Mask::operator=(Mask&& mask)
{
	if (this == &mask) return *this;
	m_radius = mask.m_radius;
	m_maskVals = std::move(mask.m_maskVals);
	m_rowPtrs = std::move(mask.m_rowPtrs);

	return *this;
}

void Mask::SetMaskValues(const std::vector<double>& mask)
{
	m_maskVals = mask;
	SetupRowPtrs();
}

void Mask::SetMaskValues(std::vector<double>&& mask)
{
	m_maskVals.swap(mask);
	SetupRowPtrs();
}

void Mask::SetupRowPtrs()
{
	m_rowPtrs.clear();
	m_rowPtrs.reserve(2 * static_cast<size_t>(m_radius) + 1);

	for (int i = 0; i < 2 * m_radius + 1; i++)
	{
		m_rowPtrs.emplace_back(&(m_maskVals[static_cast<size_t>(GetRowCount()) * i]));
	}
}

void Mask::SetRadius(int radius)
{
	m_radius = radius;
	m_maskVals.resize((2 * radius + 1) * (2 * radius + 1));
	SetupRowPtrs();
}

const double* Mask::Ptr(int row) const
{
	return m_rowPtrs[row];
}

double* Mask::Ptr(int row)
{
	return m_rowPtrs[row];
}

double Mask::GetMaximum() const
{
	double maximum = std::numeric_limits<double>::min();

	for (size_t i = 0; i < m_maskVals.size(); i++)
	{
		if (m_maskVals[i] > maximum)
		{
			maximum = m_maskVals[i];
		}
	}

	return maximum;
}

double Mask::GetMinimum() const
{
	double minimum = std::numeric_limits<double>::max();

	for (size_t i = 0; i < m_maskVals.size(); i++)
	{
		if (m_maskVals[i] < minimum)
		{
			minimum = m_maskVals[i];
		}
	}

	return minimum;
}

double Mask::GetPositiveMinimum() const
{
	double minimum = std::numeric_limits<double>::max();

	for (size_t i = 0; i < m_maskVals.size(); i++)
	{
		if (m_maskVals[i] < 0)
		{
			continue;
		}

		if (m_maskVals[i] < minimum)
		{
			minimum = m_maskVals[i];
		}
	}

	return minimum;
}

double Mask::GetNegativeMaximum() const
{
	double maximum = std::numeric_limits<double>::min();

	for (size_t i = 0; i < m_maskVals.size(); i++)
	{
		if (m_maskVals[i] > 0)
		{
			continue;
		}

		if (m_maskVals[i] > maximum)
		{
			maximum = m_maskVals[i];
		}
	}

	return maximum;
}

void Mask::MakeCircularMask(int radius, double valueInside, double valueOutside)
{
	SetRadius(radius);

	for (size_t row = 0; row < m_rowPtrs.size(); row++)
	{
		int relativeRow = static_cast<int>(row) - m_radius;

		for (size_t col = 0; col < GetColumnCount(); col++)
		{
			int relativeColumn = static_cast<int>(col) - m_radius;

			if (relativeRow * relativeRow + relativeColumn * relativeColumn <= m_radius * m_radius)
			{
				*(m_rowPtrs[row] + col) = valueInside;
			}
			else
			{
				*(m_rowPtrs[row] + col) = valueOutside;
			}
		}
	}
}

void Mask::SetMaskValuesTo(double val)
{
	for (size_t i = 0; i < m_maskVals.size(); i++)
	{
		m_maskVals[i] = val;
	}
}

/************************************/
//
//	NON UNIFORM MASK
//
//
/************************************/

void NonUniformMask::SetXRadius(int radius)
{
	m_xRadius = radius;
}

void NonUniformMask::SetYRadius(int radius)
{
	m_yRadius = radius;
}

void NonUniformMask::SetupRowPtrs()
{
	m_rowPtrs.clear();
	m_rowPtrs.reserve(2 * static_cast<size_t>(m_yRadius) + 1);

	for (int i = 0; i < 2 * m_yRadius + 1; i++)
	{
		m_rowPtrs.emplace_back(&(m_maskVals[static_cast<size_t>(2 * m_xRadius + 1) * i]));
	}
}

void NonUniformMask::SetMaskValues(const std::vector<double> &vals)
{
	if (vals.size() != (2 * m_xRadius + 1) * (2 * m_yRadius + 1))
	{
		std::cerr << "Wrong length of mask values vector\n";
		return;
	}

	m_maskVals = vals;

	SetupRowPtrs();
}


struct Params
{
	double a;
	double b;
	double c;
};

double f(double * x, size_t dim, void * p)
{
	Params *pars = static_cast<Params*>(p);

	return exp(-(pars->a * x[0] * x[0] + 2 * pars->b * x[0] * x[1] + pars->c * x[1] * x[1]));
}

void NonUniformMask::Rotate(double* A, double* B, double* C, double* D, double angle) const
{
	double cosRes = cos(angle);
	double sinRes = sin(angle);
	double temp;

	temp = cosRes * A[0] - sinRes * A[1];
	A[1] = sinRes * A[0] + cosRes * A[1];
	A[0] = temp;

	temp = cosRes * B[0] - sinRes * B[1];
	B[1] = sinRes * B[0] + cosRes * B[1];
	B[0] = temp;

	temp = cosRes * C[0] - sinRes * C[1];
	C[1] = sinRes * C[0] + cosRes * C[1];
	C[0] = temp;

	temp = cosRes * D[0] - sinRes * D[1];
	D[1] = sinRes * D[0] + cosRes * D[1];
	D[0] = temp;
}

void NonUniformMask::MaxAbs(double A[2], double B[2], double C[2], double D[2], double* Res) const
{
	double maxX = std::numeric_limits<double>::min();
	double maxY = std::numeric_limits<double>::min();
	double ax = abs(A[0]);
	double ay = abs(A[1]);
	double bx = abs(B[0]);
	double by = abs(B[1]);
	double cx = abs(C[0]);
	double cy = abs(C[1]);
	double dx = abs(D[0]);
	double dy = abs(D[1]);

	if (ax > maxX) maxX = ax;
	if (bx > maxX) maxX = bx;
	if (cx > maxX) maxX = cx;
	if (dx > maxX) maxX = dx;

	if (ay > maxY) maxY = ay;
	if (by > maxY) maxY = by;
	if (cy > maxY) maxY = cy;
	if (dy > maxY) maxY = dy;

	Res[0] = maxX;
	Res[1] = maxY;
}

void NonUniformMask::ComputeRotatedMaskDimensions(double sigmaX, double sigmaY, double angle)
{
	double A[2] = { -3 * sigmaX, -3 * sigmaY };
	double B[2] = { 3 * sigmaX, -3 * sigmaY };
	double C[2] = { 3 * sigmaX, 3 * sigmaY };
	double D[2] = { -3 * sigmaX, 3 * sigmaY };
	double max[2];

	Rotate(A, B, C, D, angle);

	A[0] = floor(A[0]);
	A[1] = floor(A[1]);
	D[0] = floor(D[0]);
	D[1] = floor(D[1]);

	B[0] = ceil(B[0]);
	B[1] = ceil(B[1]);
	C[0] = ceil(C[0]);
	C[1] = ceil(C[1]);

	MaxAbs(A, B, C, D, max);
	m_xRadius = max[0];
	m_yRadius = max[1];

	if (m_yRadius > 9) m_yRadius = 9;
}

void NonUniformMask::CreateMask(double sigmaX, double sigmaY, double angle)
{
	m_angle = angle;
	ComputeRotatedMaskDimensions(sigmaX, sigmaY, angle);

	m_maskVals.clear();
	m_maskVals.resize((2 * m_xRadius + 1) * (2 * m_yRadius + 1));
	SetupRowPtrs();

	double cosRes = cos(angle);
	double sinRes = sin(angle);
	double sin2 = sin(2 * angle);

	Params params;

	params.a = (cosRes * cosRes) / (2 * sigmaX * sigmaX) + (sinRes * sinRes) / (2 * sigmaY * sigmaY);         
	params.b = -sin2 / (4 * sigmaX * sigmaX) + sin2 / (4 * sigmaY * sigmaY);
	params.c = (sinRes * sinRes) / (2 * sigmaX * sigmaX) + (cosRes * cosRes) / (2 * sigmaY * sigmaY);

	gsl_monte_function F;

	F.f = &f;
	F.dim = 2;
	F.params = &params;

	gsl_monte_vegas_state *state = gsl_monte_vegas_alloc(2);

	const gsl_rng_type *type = gsl_rng_default;
	gsl_rng *generator = gsl_rng_alloc(type);
	gsl_rng_set(generator, time(NULL));

	double result, err;

	double lowerBounds[2] = { -0.5, -0.5 };
	double upperBounds[2] = { 0.5, 0.5 };
	int iter = 0;

	//std::cout << "MaskVals:\n";

	for (int j = -m_yRadius; j <= m_yRadius; j++)
	{
		for (int i = -m_xRadius; i <= m_xRadius; i++)
		{
			lowerBounds[0] = i - 0.5;
			lowerBounds[1] = j - 0.5;
			upperBounds[0] = i + 0.5;
			upperBounds[1] = j + 0.5;

			gsl_monte_vegas_integrate(&F, lowerBounds, upperBounds, 2, 1000, generator, state, &result, &err);

			m_maskVals[iter] = result / (2 * M_PI * sigmaX * sigmaY);

			if (m_maskVals[iter] < NEAR_ZERO) m_maskVals[iter] = 0.;

			//std::cout << m_maskVals[iter] << std::endl;
			iter++;
		}
	}

	gsl_rng_free(generator);
	gsl_monte_vegas_free(state);
}

void NonUniformMask::Print() const
{
	int iter = 0;
	double sum = 0.;

	for (int j = -m_yRadius; j <= m_yRadius; j++)
	{
		for (int i = -m_xRadius; i <= m_xRadius; i++)
		{
			sum += m_maskVals[iter];
			std::cout << m_maskVals[iter] << "\t";
			iter++;
		}

		std::cout << std::endl;
	}

	std::cout << "SUM: " << sum << std::endl;
	std::cout << std::endl;
}