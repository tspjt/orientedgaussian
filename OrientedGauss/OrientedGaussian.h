#ifndef OrientedGaussian_H
#define OrientedGaussian_H

#include "ui_OrientedGaussian.h"
#include <vtkSmartPointer.h>
#include <QMainWindow>
#include "ViewerWidget.h"
#include <QFileDialog>
#include <QString>
#include <QTreeWidgetItem>
#include <vector>
#include <QResizeEvent>
#include <QMoveEvent>
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include "GeneralTypes.h"
#include "EllipseScaleDialog.h"
#include "TensorSettingsDialog.h"

class OrientedGaussian : public QMainWindow
{
  Q_OBJECT
public:
  // Constructor/Destructor
  OrientedGaussian();
  ~OrientedGaussian() = default;

protected:
	void resizeEvent(QResizeEvent *event);
	void moveEvent(QMoveEvent *event);

private:
  // Designer form
  Ui_OrientedGaussian* ui;
  std::vector<QTreeWidgetItem*> m_treeItems;
  ViewerWidget* m_lastViewerWidget = nullptr;
  bool m_creationAllowed = true;
  EllipseScaleDialog m_ellipseScaleDialog;
  TensorSettingsDialog m_tensorSettingsDialog;

  void RemoveItemFromTree(int index);
  int FindTreeWidgetItemIndex(const QTreeWidgetItem* ptr) const;

  inline ViewerWidget* GetCurrentViewerWidget() { return static_cast<ViewerWidget*>(ui->tabWidget->currentWidget()); }
  inline ViewerWidget* GetViewerWidget(int index) { return static_cast<ViewerWidget*>(ui->tabWidget->widget(index)); }
  void OnNoMoreImages();
  void OnAtLeastOneImageExists();

public slots:
	void slotExit();
	void CreateNewTab(NewImageData imageData);
	void DisableCreation() { m_creationAllowed = false; }
	void EnableCreation() { m_creationAllowed = true; }

private slots:
	void on_actionOpen_triggered();
	void on_actionClearAll_triggered();
	void on_tabWidget_tabCloseRequested(int tabId);
	void on_tabWidget_currentChanged(int index);
	void SetActiveTreeItem(int index);
	void OnViewerWidgetTabChanged(int index);
	void on_treeWidget_itemClicked(QTreeWidgetItem* item, int col);
	void on_pushButtonSelect_pressed();
	void on_pushButtonGaussian_pressed();
	void on_pushButtonPickEllipses_toggled(bool b);
	void on_pushButtonRemoveEllipses_pressed();
	void on_actionEllipse_scale_triggered();
	void on_actionTensorSettings_triggered();
	void on_pushButtonTensor_pressed();
	void on_pushButtonLHE_pressed();
	void on_pushButtonExportVTK_pressed();
	void on_pushButtonExportPGM_pressed();
}; 

#endif
