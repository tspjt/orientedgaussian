#pragma once

#define _USE_MATH_DEFINES

#include <vector>
#include <limits>
#include <iostream>

#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_miser.h>
#include <gsl/gsl_monte_vegas.h>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <ctime>
#include <limits>
#include "GeneralTypes.h"

#define NEAR_ZERO 10e-12

class Mask
{
public:
	Mask();
	Mask(const Mask& mask);
	Mask(Mask&& mask);
	Mask& operator=(const Mask& mask);
	Mask& operator=(Mask&& mask);
	~Mask();

	void MakeCircularMask(int radius, double valueInside = 1., double valueOutside = -1.);
	const double* Ptr(int row = 0) const;
	double* Ptr(int row = 0);
	
	inline bool IsMaskSet() const { return static_cast<bool>(m_maskVals.size()); }
	inline int GetRowCount() const { return m_radius * 2 + 1; }
	inline int GetColumnCount() const { return m_radius * 2 + 1; }
	inline int GetRadius() const { return m_radius; }

	void SetRadius(int radius);
	void SetMaskValues(const std::vector<double>& mask);
	void SetMaskValues(std::vector<double>&& mask);
	void SetMaskValuesTo(double val);
	double GetMinimum() const;
	double GetMaximum() const;
	double GetPositiveMinimum() const;
	double GetNegativeMaximum() const;

protected:
	int m_radius;
	std::vector<double*> m_rowPtrs;
	std::vector<double> m_maskVals;

	void SetupRowPtrs();
};

class NonUniformMask : public Mask
{
public:
	int GetXRadius() const { return m_xRadius; }
	int GetYRadius() const { return m_yRadius; }
	void SetXRadius(int radius);
	void SetYRadius(int radius);
	void SetMaskValues(const std::vector<double> &vals);
	void CreateMask(double sigmax, double sigmay, double angle);
	double GetAngle() const { return m_angle; }
	void Print() const;

	inline int GetRowCount() const { return m_xRadius * 2 + 1; }

protected:
	int m_xRadius;
	int m_yRadius;
	double m_angle;

	void SetupRowPtrs();

private:
	void ComputeRotatedMaskDimensions(double sigmaX, double sigmaY, double angle);
	void Rotate(double *A, double *B, double *C, double* D, double angle) const;
	void MaxAbs(double A[2], double B[2], double C[2], double D[2], double* Res) const;
};