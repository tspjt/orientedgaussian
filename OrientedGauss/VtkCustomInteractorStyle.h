#pragma once

#include <QtCore/QtCore>
#include <vtkImageViewer2.h>
#include <vtkSmartPointer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkImageActor.h>
#include <vtkCornerAnnotation.h>
#include <vtkInteractorStyle.h>
#include <sstream>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkInteractorStyleImage.h>
#include <vtkCornerAnnotation.h>
#include <vtkCoordinate.h>
#include <vtkCamera.h>
#include "NewImageDialog.h"
#include <vtkExtractVOI.h>
#include <vtkImageMapper3D.h>

class VtkCustomInteractorStyle : public vtkInteractorStyleImage
{
public:
	void SetImageViewer(vtkSmartPointer<vtkImageViewer2> viewer) { m_imageViewer = viewer; }
	void SetImageRenderer(vtkSmartPointer<vtkRenderer> renderer) { m_imageRenderer = renderer; }
	void SetInfoRenderer(vtkSmartPointer<vtkRenderer> renderer);	
	void SetCornerAnnotation(vtkSmartPointer<vtkCornerAnnotation> annotation) { m_cornerAnnotation = annotation; }
	void SetRenderWindow(vtkSmartPointer<vtkRenderWindow> renderWindow) { m_renderWindow = renderWindow; }
	void SetInteractor(vtkSmartPointer<vtkRenderWindowInteractor> interactor) { m_interactor = interactor; }
	void SetDialog(NewImageDialog* dialog) { m_newImageDialog = dialog; }
	void SetOriginalSource(vtkSmartPointer<vtkImageData> originalSource) { m_originalSource = originalSource; }

	void SelectionOn() { m_selectionOn = true; }
	void SelectionOff() { m_selectionOn = false; }
	void EllipseOn() { m_ellipseOn = true; }
	void EllipseOff() { m_ellipseOn = false; }
	void OnLeftButtonDown() override;
	void OnLeftButtonUp() override;
	void OnMouseMove() override;
	void OnKeyDown() override;
	void ResetSelection();

	void InitInfoRenderer();

	int MouseClick;
	
	static VtkCustomInteractorStyle *New();

	vtkTypeMacro(VtkCustomInteractorStyle, vtkInteractorStyleImage);

protected:
	VtkCustomInteractorStyle();
	~VtkCustomInteractorStyle();

private:
	NewImageDialog* m_newImageDialog;
	vtkSmartPointer<vtkImageData> m_originalSource;
	vtkSmartPointer<vtkImageViewer2> m_imageViewer;
	vtkSmartPointer<vtkRenderer> m_imageRenderer;
	vtkSmartPointer<vtkRenderer> m_infoRenderer;
	vtkSmartPointer<vtkPolyData> m_rectangle;
	vtkSmartPointer<vtkCornerAnnotation> m_cornerAnnotation;
	vtkSmartPointer<vtkPolyDataMapper> m_polygonMapper;
	vtkSmartPointer<vtkRenderWindowInteractor> m_interactor;
	vtkSmartPointer<vtkRenderWindow> m_renderWindow;
	bool m_isLeftMouseButtonPressed = false;
	bool m_selectionOn = false;
	bool m_ellipseOn = false;
	double m_xStart, m_yStart;
	double m_xEnd, m_yEnd;	

	VtkCustomInteractorStyle(const VtkCustomInteractorStyle&);
	void operator=(const VtkCustomInteractorStyle&);
	void GetCursorPosition(double coords[2], int* coordsInt = nullptr);
	bool IsSelectionViable(double width, double height) const;
	void SortSelectionRectangle(double &xStart, double &yStart, double &xEnd, double &yEnd) const;
	void ClipSelectionToImage(double &m_xStart, double &m_yStart, double &m_xEnd, double &m_yEnd, double width, double height) const;
};

