#include "Operation.h"

double gPM(double s, double KK)
{
	double ss(sqrt(s) * 255.);

	return 1. / (1. + KK * ss * ss);
}

Operation::Operation()
{

}

void Operation::ResizeOutputImageIfNeeded(const NImage& from, NImage& to) const
{
	if (from.Height() == to.Height() && from.Width() == to.Width()) return;

	to = from;
}

void Operation::ResizeOutputImageIfNeeded(const QImage& from, NImage& to) const
{
	if (from.height() == to.Height() && from.width() == to.Width()) return;

	to = NImage(from);
}

void Operation::ResizeOutputImageIfNeeded(const QImage& from, QImage& to) const
{
	if (from.height() == to.height() && from.width() == to.width()) return;

	to = from;
}

double Operation::Mean(const NImage& img) const
{
	double accum = 0.;
	
	for (int i = 0; i < img.GetNumOfPixels(); i++)
	{
		accum += img.Bits()[i];
	}

	return accum / img.GetNumOfPixels();
}

double Operation::SumNeighbourValues(int row, int column, const NImage& img) const
{
	int rowLength = img.Width();
	const double *rowPtr = img.Ptr(row);

	double east = rowPtr[column + 1];
	double north = rowPtr[column - rowLength];
	double west = rowPtr[column - 1];
	double south = rowPtr[column + rowLength];

	return east + north + west + south;
}

double Operation::L2NormSquared(const NImage& img) const
{
	double norm = 0.;

	for (size_t i = 0; i < img.GetNumOfPixels(); i++)
	{
		norm += img.Bits()[i] * img.Bits()[i];
	}

	return norm;
}

/***************************************/
//
//
//			LHE_IMPLICIT
//
//
/***************************************/

OperationLHEImplicit::OperationLHEImplicit() : Operation()
{

}

void OperationLHEImplicit::GaussSeidel()
{
	for (size_t row = 1; row < m_gaussSeidelVar.Height() - 1; row++)
	{
		double* rowPtr = m_gaussSeidelVar.Ptr(row);
		double* uOldTimeRow = m_iterInput.Ptr(row);

		for (size_t col = 1; col < m_gaussSeidelVar.Width() - 1; col++)
		{
			rowPtr[col] = (uOldTimeRow[col] + SumNeighbourValues(row, col, m_gaussSeidelVar) * m_aq) / m_ap;
		}
	}
}

void OperationLHEImplicit::DoSORIteration()
{
	GaussSeidel();

	m_sorIterResult = m_sorIterResult + (m_gaussSeidelVar - m_sorIterResult) * m_omega;
	m_sorIterResult.RemoveEdges();
	m_sorIterResult.MirrorEdges(1);
}

void OperationLHEImplicit::SOR()
{
	while (m_res > m_tol * m_tol)
	{
		DoSORIteration();

		NImage residual(m_sorIterResult.RemovedEdges());

		for (size_t row = 1; row < m_sorIterResult.Height() - 1; row++)
		{
			double* resRowPtr = residual.Ptr(row - 1);
			const double* sorRowPtr = m_sorIterResult.Ptr(row);
			const double* lastIterRowPtr = m_iterInput.Ptr(row);

			for (size_t col = 1; col < m_sorIterResult.Width() - 1; col++)
			{
				resRowPtr[col - 1] = m_ap * sorRowPtr[col] - m_aq * SumNeighbourValues(row, col, m_sorIterResult) - lastIterRowPtr[col];
			}
		}

		m_res = L2NormSquared(residual);
		m_gaussSeidelVar = m_sorIterResult;
	}

	m_res = std::numeric_limits<double>::max();
}

void OperationLHEImplicit::DoIteration()
{
	SOR();

	m_iterResult = m_sorIterResult.RemovedEdges();
}

void OperationLHEImplicit::Do(const OperationLHEImplicitParams &params)
{
	m_tol = params.tolerance;
	m_omega = params.omega;
	m_tau = params.timeStep;
	m_ap = 1 + 4 * m_tau;
	m_aq = m_tau;
	m_iterInput = params.from->MirroredEdges(1);
	m_gaussSeidelVar = m_iterInput;
	m_sorIterResult = m_iterInput;
	m_iterResult = NImage(params.from->Height(), params.from->Width());
	double lastMean = Mean(m_iterInput);

	for (int i = 0; i < params.numOfSteps; i++)
	{
		DoIteration();
		m_iterInput = m_iterResult.MirroredEdges(1);
		m_gaussSeidelVar = m_iterInput;
		m_sorIterResult = m_iterInput;

		double newMean = Mean(m_iterInput);
		qDebug() << "Old: " << lastMean << " new: " << newMean;

		if (static_cast<int>(newMean * 1000) != static_cast<int>(lastMean * 1000))
		{
			qDebug() << "Means are different";
		}

		lastMean = newMean;	
	}
	
	*(params.to) = m_iterResult;
}
