#include "EllipseScaleDialog.h"

EllipseScaleDialog::EllipseScaleDialog(QWidget* parent) : QDialog(parent), ui(new Ui::EllipseScaleDialog)
{
	ui->setupUi(this);
}

void EllipseScaleDialog::on_pushButtonOK_pressed()
{
	emit accepted();
	emit Accepted(ui->doubleSpinBox->value());
	close();
}


void EllipseScaleDialog::on_pushButtonCancel_pressed()
{
	emit rejected();
	close();
}

void EllipseScaleDialog::on_horizontalSlider_sliderMoved(int val)
{
	ui->doubleSpinBox->setValue(0.5 * val);
}

void EllipseScaleDialog::on_doubleSpinBox_valueChanged(double val)
{
	ui->horizontalSlider->setValue(static_cast<int>(val / 0.5));
}