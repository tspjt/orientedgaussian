#include "OrientedGaussian.h"

// This is included here because it is forward declared in
// OrientedGaussian.h

#if VTK_VERSION_NUMBER >= 89000000000ULL
#define VTK890 1
#endif

// Constructor
OrientedGaussian::OrientedGaussian()
{
	this->ui = new Ui_OrientedGaussian;
	this->ui->setupUi(this);

	OnNoMoreImages();
	connect(this->ui->actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));
}

/*********************************/
//
//
//		PUBLIC METHODS
//
/*********************************/



/*********************************/
//
//
//		PROTECTED METHODS
//
/*********************************/

void OrientedGaussian::resizeEvent(QResizeEvent *event)
{
	QSize tab = event->size();
	QSize gl = event->size();

	tab.setHeight(tab.height() - 50);
	tab.setWidth(tab.width() - 180);

	gl.setHeight(tab.height() - 60);
	gl.setWidth(tab.width() - 60);

	QMainWindow::resizeEvent(event);
	ui->tabWidget->resize(tab);
	ui->GLWidget->resize(gl);
}

void OrientedGaussian::moveEvent(QMoveEvent *event)
{
	QMainWindow::moveEvent(event);
}


/*********************************/
//
//
//		PRIVATE METHODS
//
/*********************************/

void OrientedGaussian::RemoveItemFromTree(int index)
{
	ui->treeWidget->removeItemWidget(m_treeItems[index], 0);
	delete m_treeItems[index];
	m_treeItems.erase(m_treeItems.begin() + index);
}

int OrientedGaussian::FindTreeWidgetItemIndex(const QTreeWidgetItem* ptr) const
{
	for (size_t i = 0; i < m_treeItems.size(); i++)
	{
		if (m_treeItems[i] == ptr) return static_cast<int>(i);
	}

	return -1;
}

/*********************************/
//
//
//		PUBLIC SLOTS
//
/*********************************/


void OrientedGaussian::slotExit()
{
  qApp->exit();
}


/*********************************/
//
//
//		PRIVATE SLOTS
//
/*********************************/

void OrientedGaussian::CreateNewTab(NewImageData imageData)
{
	if (!m_creationAllowed) return;

	QTreeWidgetItem *image = new QTreeWidgetItem(ui->treeWidget);
	m_treeItems.push_back(image);
	QString imageName = imageData.imageName;
	image->setText(0, imageName);
	image->setExpanded(true);

	QTreeWidgetItem *sub = new QTreeWidgetItem(image);
	sub->setText(0, "Original");
	sub = new QTreeWidgetItem(image);
	sub->setText(0, "Result");

	ViewerWidget* widget = new ViewerWidget(nullptr, ui->GLWidget, this);
	widget->SetOriginalImage(imageData.data);
	ui->tabWidget->addTab(widget, imageName);
	ui->tabWidget->setCurrentWidget(widget);
}

void OrientedGaussian::on_actionOpen_triggered()
{	
	QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
	QString fileName = QFileDialog::getOpenFileName(this, "Open image", "./", fileFilter);
	QString imageName = fileName.split('/').back();
	if (fileName.isEmpty()) { return; }

	QTreeWidgetItem *image = new QTreeWidgetItem(ui->treeWidget);
	m_treeItems.push_back(image);
	image->setText(0, imageName);
	image->setExpanded(true);

	QTreeWidgetItem *sub = new QTreeWidgetItem(image);
	sub->setText(0, "Original");
	sub = new QTreeWidgetItem(image);
	sub->setText(0, "Result");

	ViewerWidget* widget = new ViewerWidget(nullptr, ui->GLWidget, this);
	widget->SetOriginalImage(fileName);
	ui->tabWidget->addTab(widget, imageName);
	ui->tabWidget->setCurrentWidget(widget);
}

void OrientedGaussian::on_actionClearAll_triggered()
{
	for (int i = ui->tabWidget->count() - 1; i >= 0; i--)
	{
		ui->tabWidget->widget(i)->deleteLater();
		ui->tabWidget->removeTab(i);
		RemoveItemFromTree(i);
	}
}

void OrientedGaussian::on_tabWidget_tabCloseRequested(int tabId)
{
	ViewerWidget *vw = static_cast<ViewerWidget*>(ui->tabWidget->widget(tabId));	

	if (m_lastViewerWidget == vw) m_lastViewerWidget = nullptr;

	vw->deleteLater();
	RemoveItemFromTree(tabId);
}

void OrientedGaussian::OnNoMoreImages()
{
	ui->GLWidget->hide();
	ui->toolBox->setEnabled(false);
}

void OrientedGaussian::OnAtLeastOneImageExists()
{
	ui->GLWidget->show();
	ui->toolBox->setEnabled(true);
}

void OrientedGaussian::on_tabWidget_currentChanged(int index)
{
	ui->pushButtonSelect->setEnabled(true);
	ui->pushButtonPickEllipses->setChecked(false);

	if (m_lastViewerWidget != nullptr)
	{
		m_lastViewerWidget->CleanUpForNextWidget();
		disconnect(m_lastViewerWidget, SIGNAL(TabChanged(int)), this, SLOT(OnViewerWidgetTabChanged(int)));
	}

	m_lastViewerWidget = GetCurrentViewerWidget();

	if (m_lastViewerWidget == nullptr)
	{
		OnNoMoreImages();	
		return;
	}
	
	OnAtLeastOneImageExists();
	m_lastViewerWidget->TakeOverRenderWindow();
	SetActiveTreeItem(index);
	connect(m_lastViewerWidget, SIGNAL(TabChanged(int)), this, SLOT(OnViewerWidgetTabChanged(int)));
	connect(&m_ellipseScaleDialog, SIGNAL(Accepted(double)), m_lastViewerWidget, SLOT(SetEllipseScale(double)));
}

void OrientedGaussian::SetActiveTreeItem(int index)
{	
	ui->treeWidget->clearSelection();
	ViewerWidget* current = GetViewerWidget(index);

	if (current == nullptr) return;

	QTreeWidgetItem *item = m_treeItems[index];
	int childIndex = current->GetCurrentTabIndex();

	if (childIndex == 2)
	{
		item->setSelected(true);
		return;
	}

	item->child(childIndex)->setSelected(true);
}

void OrientedGaussian::OnViewerWidgetTabChanged(int index)
{
	ui->treeWidget->clearSelection();
	int childIndex = index;

	if (childIndex == 2)
	{
		ui->pushButtonGaussian->setEnabled(false);
		ui->pushButtonPickEllipses->setEnabled(false);
		ui->pushButtonRemoveEllipses->setEnabled(false);
		ui->pushButtonSelect->setEnabled(true);
		m_treeItems[ui->tabWidget->currentIndex()]->setSelected(true);
		return;
	}

	ui->pushButtonGaussian->setEnabled(true);
	ui->pushButtonPickEllipses->setEnabled(true);
	ui->pushButtonRemoveEllipses->setEnabled(true);
	ui->pushButtonSelect->setEnabled(true);
	ui->pushButtonPickEllipses->setChecked(false);
	m_treeItems[ui->tabWidget->currentIndex()]->child(childIndex)->setSelected(true);
}

void OrientedGaussian::on_treeWidget_itemClicked(QTreeWidgetItem* item, int col)
{
	int tabIndex;
	int widgetTabIndex;

	if (item->childCount() == 0)
	{
		tabIndex = FindTreeWidgetItemIndex(item->parent());			
		if (item->text(0) == "Original")
		{
			widgetTabIndex = 0;
		}
		else
		{
			widgetTabIndex = 1;
		}
	}
	else
	{
		tabIndex = FindTreeWidgetItemIndex(item);
		widgetTabIndex = 2;
	}
	 
	ui->tabWidget->setCurrentIndex(tabIndex);
	GetViewerWidget(tabIndex)->SwitchTabs(widgetTabIndex);
}

void OrientedGaussian::on_pushButtonSelect_pressed()
{
	GetCurrentViewerWidget()->SelectionOn();
}

void OrientedGaussian::on_pushButtonGaussian_pressed()
{
	GetCurrentViewerWidget()->Gaussian(ui->spinBoxGaussIter->value());	
}

void OrientedGaussian::on_pushButtonTensor_pressed()
{
	GetCurrentViewerWidget()->Tensor(ui->spinBoxTensorIter->value());
}

void OrientedGaussian::on_pushButtonPickEllipses_toggled(bool b)
{
	if (b)
	{
		GetCurrentViewerWidget()->EllipseOn();
		ui->pushButtonSelect->setEnabled(false);
	}
	else
	{
		GetCurrentViewerWidget()->EllipseOff();
		ui->pushButtonSelect->setEnabled(true);
	}
}

void OrientedGaussian::on_pushButtonRemoveEllipses_pressed()
{
	GetCurrentViewerWidget()->RemoveEllipses();
}

void OrientedGaussian::on_actionEllipse_scale_triggered()
{
	m_ellipseScaleDialog.exec();
}

void OrientedGaussian::on_actionTensorSettings_triggered()
{
	m_tensorSettingsDialog.exec();

	if (m_tensorSettingsDialog.Confirmed())
	{
		GetCurrentViewerWidget()->SetLHESourceParams(m_tensorSettingsDialog.GetOriginalLHEParams());
		GetCurrentViewerWidget()->SetLHEAParams(m_tensorSettingsDialog.GetACoefLHEParams());
		GetCurrentViewerWidget()->SetLHEBParams(m_tensorSettingsDialog.GetBCoefLHEParams());
		GetCurrentViewerWidget()->SetLHECParams(m_tensorSettingsDialog.GetCCoefLHEParams());

		GetCurrentViewerWidget()->SetCParam(m_tensorSettingsDialog.GetCParam());
		GetCurrentViewerWidget()->SetLambda(m_tensorSettingsDialog.GetLambda());
	}
}

void OrientedGaussian::on_pushButtonLHE_pressed()
{	
	GetCurrentViewerWidget()->LHECustom(m_tensorSettingsDialog.GetOriginalLHEParams(), ui->spinBoxLHEIter->value());
}

void OrientedGaussian::on_pushButtonExportVTK_pressed()
{
	GetCurrentViewerWidget()->ExportImageToVtk("result.vtk");
}

void OrientedGaussian::on_pushButtonExportPGM_pressed()
{
	GetCurrentViewerWidget()->ExportImageToPGM("result.pgm");
}