#include "TensorSettingsDialog.h"


#include "EllipseScaleDialog.h"

TensorSettingsDialog::TensorSettingsDialog(QWidget* parent) : QDialog(parent), ui(new Ui::TensorSettingsDialog)
{
	ui->setupUi(this);
}

void TensorSettingsDialog::on_pushButtonOK_pressed()
{
	m_bConfirmed = true;

	m_originalParams.numOfSteps = ui->spinBoxNumOfStepsOriginal->value();
	m_originalParams.timeStep = ui->doubleSpinBoxTimeStepOriginal->value();
	m_originalParams.omega = ui->doubleSpinBoxOmegaOriginal->value();

	m_aCoef.numOfSteps = ui->spinBoxNumOfSteps->value();
	m_aCoef.timeStep = ui->doubleSpinBoxTimeStep->value();
	m_aCoef.omega = ui->doubleSpinBoxOmegaA->value();

	m_bCoef.numOfSteps = ui->spinBoxNumOfSteps->value();
	m_bCoef.timeStep = ui->doubleSpinBoxTimeStep->value();
	m_bCoef.omega = ui->doubleSpinBoxOmegaB->value();

	m_cCoef.numOfSteps = ui->spinBoxNumOfSteps->value();
	m_cCoef.timeStep = ui->doubleSpinBoxTimeStep->value();
	m_cCoef.omega = ui->doubleSpinBoxOmegaC->value();

	m_cParam = ui->doubleSpinBoxCParam->value();
	m_lambda = ui->doubleSpinBoxLambdaParam->value();

	close();
}