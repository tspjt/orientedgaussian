#include "NewImageDialog.h"

NewImageDialog::NewImageDialog(QWidget* parent) : QDialog(parent), ui(new Ui::NewImageDialog)
{
	ui->setupUi(this);

	ui->lineEditImageName->selectAll();
}

void NewImageDialog::on_pushButtonOK_pressed()
{
	m_imageData.imageName = ui->lineEditImageName->text();
	emit accepted();
	emit AcceptedSelection(m_imageData);
	emit Done();
	close();
}

void NewImageDialog::Exec()
{
	emit Start();
	exec();
}

void NewImageDialog::on_pushButtonCancel_pressed()
{
	emit rejected();
	emit Done();
	close();
}