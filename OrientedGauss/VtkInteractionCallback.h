#pragma once
#define _USE_MATH_DEFINES

#include <vtkCommand.h>
#include <vtkObjectFactory.h>
#include <vtkOpenGLRenderer.h>
#include <vtkEllipseArcSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vector>
#include "GeneralTypes.h"
#include <vtkTransform.h>
#include <vtkVector.h>
#include <cmath>
#include "Mask.h"
#include <iomanip>

#define EIGEN_MUCH_MORE 300
#define EIGEN_ZERO_TOL 10

class VtkInteractionCallback : public vtkCommand
{
public:
	void SetRenderer(vtkSmartPointer<vtkOpenGLRenderer> renderer) { m_renderer = renderer; }
	void SetGaussData(std::vector<GaussianData> *data) { m_gaussData = data; }
	void SetWidth(int width) { m_width = width; }
	void SetHeight(int height) { m_height = height; }
	void SetScale(double scale) { m_scale = scale; }

	static VtkInteractionCallback *New();
	virtual void Execute(vtkObject *, unsigned long vtkNotUsed(event), void *callData);

	vtkTypeMacro(VtkInteractionCallback, vtkCommand);

protected:
	VtkInteractionCallback();
	~VtkInteractionCallback();

private:
	vtkSmartPointer<vtkOpenGLRenderer> m_renderer = nullptr;
	std::vector<GaussianData> * m_gaussData;
	int m_width;
	int m_height;
	double m_scale = 1.;

	VtkInteractionCallback(const VtkInteractionCallback&);
	void operator=(const VtkInteractionCallback&);
};

