#include "NImage.h"

NImage::NImage()
{
	m_bits = nullptr;
	m_rowStart = nullptr;
	m_height = 0;
	m_width = 0;
}

NImage::NImage(const QImage *img)
{
	int size = img->width() * img->height();
	const uchar *bits = img->bits();
	m_bits = new double[size];
	m_height = img->height();
	m_width = img->width();
	m_rowStart = nullptr;
	
	size_t iter = 0;

	for (int row = 0; row < m_height; row++)
	{
		for (int col = 0; col < m_width; col++)
		{
			m_bits[iter] = bits[row * img->bytesPerLine() + col] / 255.;
			iter++;
		}
	}

	SetupRowPtrs();
}

NImage::NImage(const QImage &img)
{
	int size = img.width() * img.height();
	const uchar *bits = img.bits();
	m_bits = new double[size];
	m_height = img.height();
	m_width = img.width();
	m_rowStart = nullptr;

	size_t iter = 0;

	for (int row = 0; row < m_height; row++)
	{
		for (int col = 0; col < m_width; col++)
		{
			m_bits[iter] = bits[row * img.bytesPerLine() + col] / 255.;
			iter++;
		}
	}

	SetupRowPtrs();
}

NImage::NImage(int height, int width, double val)
{
	int size = height * width;
	m_bits = new double[size];
	m_height = height;
	m_width = width;
	m_rowStart = nullptr;

	for (int i = 0; i < size; i++)
	{
		m_bits[i] = val;
	}

	SetupRowPtrs();
}

NImage::NImage(int height, int width, const double* arr)
{
	int size = height * width;
	m_bits = new double[size];
	m_height = height;
	m_width = width;
	m_rowStart = nullptr;

	for (int i = 0; i < size; i++)
	{
		m_bits[i] = arr[i];
	}

	SetupRowPtrs();
}

NImage::NImage(vtkImageData* data)
{
	int dims[3];
	data->GetDimensions(dims);

	size_t iter = 0;
	int size = dims[0] * dims[1];
	m_bits = new double[size];
	m_width = dims[0];
	m_height = dims[1];
	m_rowStart = nullptr;

	unsigned char* arr = static_cast<unsigned char*>(data->GetScalarPointer());
	int scalar = data->GetNumberOfScalarComponents();

	for (int i = 0; i < size; i++)
	{
		m_bits[i] = arr[iter] / 255.;
		iter += scalar;
	}

	SetupRowPtrs();

	Mirror(false, true);
}

NImage::NImage(const NImage &img)
{
	m_height = img.m_height;
	m_width = img.m_width;
	int size = m_height * m_width;
	m_bits = new double[size];
	m_rowStart = nullptr;
	m_mirroredEdgeWidth = img.m_mirroredEdgeWidth;

	for (int i = 0; i < size; i++)
	{
		m_bits[i] = img.m_bits[i];
	}

	SetupRowPtrs();
}

NImage::NImage(NImage &&img)
{
	m_height = img.m_height;
	m_width = img.m_width;
	m_bits = img.m_bits;
	m_rowStart = img.m_rowStart;
	m_mirroredEdgeWidth = img.m_mirroredEdgeWidth;

	img.m_bits = nullptr;
	img.m_rowStart = nullptr;
}

NImage::NImage(const QString& path)
{
	std::ifstream in(path.toStdString());
	int height, width;

	in >> height >> width;

	NImage temp(height, width);

	for (int i = 0; i < temp.GetNumOfPixels(); i++)
	{
		in >> temp.Bits()[i];
	}

	in.close();

	*this = std::move(temp);
}

NImage NImage::FromQImageKeepValues(const QImage *img)
{
	NImage nImg;

	int size = img->width() * img->height();
	const uchar *bits = img->bits();
	nImg.m_bits = new double[size];
	nImg.m_height = img->height();
	nImg.m_width = img->width();
	nImg.m_rowStart = nullptr;

	size_t iter = 0;

	for (int row = 0; row < nImg.m_height; row++)
	{
		for (int col = 0; col < nImg.m_width; col++)
		{
			nImg.m_bits[iter] = bits[row * img->bytesPerLine() + col];
			iter++;
		}
	}

	nImg.SetupRowPtrs();

	return std::move(nImg);
}

NImage& NImage::operator=(const NImage &img)
{
	if (img.Width() != m_width || img.Height() != m_height)
		if (m_bits != nullptr)
		{
			delete[] m_bits;
			m_bits = nullptr;
		}

	m_height = img.m_height;
	m_width = img.m_width;
	int size = m_height * m_width;

	if (m_bits == nullptr)
		m_bits = new double[size];	

	m_mirroredEdgeWidth = img.m_mirroredEdgeWidth;

	for (int i = 0; i < size; i++)
	{
		m_bits[i] = img.m_bits[i];
	}

	SetupRowPtrs();

	return *this;
}

NImage& NImage::operator=(NImage &&img)
{
	if (this == &img) return *this;
	if (m_bits != nullptr) delete[] m_bits;
	if (m_rowStart != nullptr) delete[] m_rowStart;

	m_height = img.m_height;
	m_width = img.m_width;
	m_bits = img.m_bits;
	m_rowStart = img.m_rowStart;
	m_mirroredEdgeWidth = img.m_mirroredEdgeWidth;

	img.m_bits = nullptr;
	img.m_rowStart = nullptr;

	return *this;
}

NImage NImage::operator+(const NImage& img) const
{
	NImage temp(*this);

	for (size_t i = 0; i < temp.GetNumOfPixels(); i++)
	{
		temp.m_bits[i] += img.m_bits[i];
	}

	return temp;
}

NImage NImage::operator-(const NImage& img) const
{
	NImage temp(*this);

	for (size_t i = 0; i < temp.GetNumOfPixels(); i++)
	{
		temp.m_bits[i] -= img.m_bits[i];
	}

	return temp;
}

NImage NImage::operator*(double val) const
{
	NImage temp(*this);

	for (size_t i = 0; i < temp.GetNumOfPixels(); i++)
	{
		temp.m_bits[i] *= val;
	}

	return temp;
}

NImage NImage::operator/(double val) const
{
	NImage temp(*this);

	for (size_t i = 0; i < temp.GetNumOfPixels(); i++)
	{
		temp.m_bits[i] /= val;
	}

	return temp;
}

double* NImage::operator[](int index)
{
	return m_rowStart[index];
}

const double* NImage::operator[](int index) const
{
	return m_rowStart[index];
}

NImage::~NImage()
{
	if (m_bits != nullptr) delete[] m_bits;
	if (m_rowStart != nullptr) delete[] m_rowStart;
	m_bits = nullptr;
	m_rowStart = nullptr;
}

void NImage::SetupRowPtrs()
{
	if (m_bits == nullptr) return;
	if (m_rowStart != nullptr) delete[] m_rowStart;

	m_rowStart = new double*[m_width * m_height];

	for (int i = 0; i < m_height; i++)
	{
		m_rowStart[i] = &(m_bits[i * m_width]);
	}
}

double* NImage::Ptr(int row)
{
	return m_rowStart[row];
}

const double* NImage::Ptr(int row) const
{
	return m_rowStart[row];
}

double* NImage::Bits()
{
	return m_bits;
}

const double* NImage::Bits() const
{
	return m_bits;
}

void NImage::MirrorHorizontally()
{
	if (m_bits == nullptr) return;

	double* buff = new double[m_height * m_width];

	for (int row = 0; row < m_height; row++)
	{
		for (int col = 0; col < m_width; col++)
		{
			buff[row * m_width + m_width - (col + 1)] = m_bits[row * m_width + col];
		}
	}

	delete[] m_bits;
	m_bits = buff;

	SetupRowPtrs();
}

void NImage::MirrorVertically()
{
	if (m_bits == nullptr) return;

	double* buff = new double[m_height * m_width];

	for (int row = 0; row < m_height; row++)
	{
		for (int col = 0; col < m_width; col++)
		{
			buff[(m_height - row - 1) * m_width + col] = m_bits[row * m_width + col];
		}
	}

	delete[] m_bits;
	m_bits = buff;

	SetupRowPtrs();
}

void NImage::Mirror(bool horizontally, bool vertically)
{
	if (horizontally) MirrorHorizontally();
	if (vertically) MirrorVertically();
}

NImage NImage::Mirrored(bool horizontally, bool vertically)
{
	NImage temp(*this);

	temp.Mirror(horizontally, vertically);

	return std::move(temp);
}

void NImage::ConvertToQImage(QImage* img) const
{
	(*img) = QImage(m_width, m_height, QImage::Format_Grayscale8);
	img->fill(Qt::black);

	size_t index = 0;

	for (int row = 0; row < img->height(); row++)
	{
		for (int col = 0; col < img->width(); col++)
		{		
			img->bits()[row * img->bytesPerLine() + col] = static_cast<uchar>(round(m_bits[index] * 255.));
			index++;
		}
	}
}

QImage NImage::GetQImage() const
{
	QImage img;

	ConvertToQImage(&img);

	return img;
}

NImage NImage::Copy(const QRect& rect) const 
{
	NImage temp(rect.height(), rect.width());

	for (int tRow = 0, row = rect.y(); row <= rect.bottom(); tRow++, row++)
	{
		for (int tCol = 0, col = rect.x(); col <= rect.right(); tCol++, col++)
		{
			temp.Bits()[tRow * temp.m_width + tCol] = m_bits[row * m_width + col];
		}
	}

	return temp;
}

void NImage::ComputeGradients()
{
	NImage buff = this->MirroredEdges(1);
	m_gradients.clear();
	m_gradients.resize(m_height * m_width);
	m_gradientNorms.clear();
	m_gradientNorms.resize(m_height * m_width);

	int iter = 0;

	for (int j = 1; j <= m_height; j++)
	{
		for (int i = 1; i <= m_width; i++)
		{
			m_gradients[iter].ux = (buff.m_bits[j * buff.m_width + i + 1] - buff.m_bits[j * buff.m_width + i - 1]) / 2.;
			m_gradients[iter].uy = (buff.m_bits[(j + 1) * buff.m_width + i] - buff.m_bits[(j - 1) * buff.m_width + i]) / 2.;
			m_gradientNorms[iter] = sqrt(m_gradients[iter].ux * m_gradients[iter].ux + m_gradients[iter].uy * m_gradients[iter].uy);

			if (m_gradientNorms[iter] < NEAR_ZERO) m_gradientNorms[iter] = 0.;

			iter++;
		}
	}
}

void NImage::ComputeGradientsScaled()
{
	NImage buff = this->MirroredEdges(1);
	m_gradients.clear();
	m_gradients.resize(m_height * m_width);
	m_gradientNorms.clear();
	m_gradientNorms.resize(m_height * m_width);

	int iter = 0;

	for (int j = 1; j <= m_height; j++)
	{
		for (int i = 1; i <= m_width; i++)
		{
			m_gradients[iter].ux = (buff.m_bits[j * buff.m_width + i + 1] - buff.m_bits[j * buff.m_width + i - 1]) * 255. / 2.;
			m_gradients[iter].uy = (buff.m_bits[(j + 1) * buff.m_width + i] - buff.m_bits[(j - 1) * buff.m_width + i]) * 255. / 2.;
			m_gradientNorms[iter] = sqrt((m_gradients[iter].ux * m_gradients[iter].ux + m_gradients[iter].uy * m_gradients[iter].uy) * 255);

			if (m_gradientNorms[iter] < NEAR_ZERO) m_gradientNorms[iter] = 0.;
			if (std::abs(m_gradients[iter].ux) < NEAR_ZERO) m_gradients[iter].ux = 0.;
			if (std::abs(m_gradients[iter].uy) < NEAR_ZERO) m_gradients[iter].uy = 0.;

			iter++;
		}
	}
}

void NImage::MirrorEdges(int edgeWidth)
{
	if (edgeWidth > m_height || edgeWidth > m_width)
	{
		qDebug() << "Edge width set bigger than the size of the image";
		return;
	}

	NImage buff(m_height + 2 * edgeWidth, m_width + 2 * edgeWidth);
	buff.m_mirroredEdgeWidth = edgeWidth;
	NImage temp;
	QRect rect;	

	//CENTER
	InsertImage(*this, buff, edgeWidth, edgeWidth);

	//TOP
	rect.setCoords(0, 0, m_width - 1, edgeWidth - 1);
	temp = this->Copy(rect);
	temp.Mirror(false, true);
	InsertImage(temp, buff, edgeWidth, 0);

	//BOT
	rect.setCoords(0, m_height - edgeWidth, m_width - 1, m_height - 1);
	temp = this->Copy(rect);
	temp.Mirror(false, true);
	InsertImage(temp, buff, edgeWidth, edgeWidth + m_height);

	//LEFT
	rect.setCoords(0, 0, edgeWidth, m_height - 1);
	temp = this->Copy(rect);
	temp.Mirror(true, false);
	InsertImage(temp, buff, 0, edgeWidth);

	//RIGHT
	rect.setCoords(m_width - edgeWidth, 0, m_width - 1, m_height - 1);
	temp = this->Copy(rect);
	temp.Mirror(true, false);
	InsertImage(temp, buff, edgeWidth + m_width, edgeWidth);

	//TOPLEFT
	rect.setCoords(0, 0, edgeWidth - 1, edgeWidth - 1);
	temp = this->Copy(rect);
	temp.Mirror(true, true);
	InsertImage(temp, buff, 0, 0);

	//TOPRIGHT
	rect.setCoords(m_width - edgeWidth, 0, m_width - 1, edgeWidth - 1);
	temp = this->Copy(rect);
	temp.Mirror(true, true);
	InsertImage(temp, buff, edgeWidth + m_width, 0);

	//BOTLEFT
	rect.setCoords(0, m_height - edgeWidth, edgeWidth - 1, m_height - 1);
	temp = this->Copy(rect);
	temp.Mirror(true, true);
	InsertImage(temp, buff, 0, edgeWidth + m_height);

	//BOTRIGHT
	rect.setCoords(m_width - edgeWidth, m_height - edgeWidth, m_width - 1, m_height - 1);
	temp = this->Copy(rect);
	temp.Mirror(true, true);
	InsertImage(temp, buff, edgeWidth + m_width, edgeWidth + m_height);

	*this = std::move(buff);
}

NImage NImage::MirroredEdges(int edgeWidth) const
{
	NImage buff(*this);

	buff.MirrorEdges(edgeWidth);

	return std::move(buff);
}

void NImage::RemoveEdges()
{
	QRect rect(m_mirroredEdgeWidth, m_mirroredEdgeWidth, m_width - 2 * m_mirroredEdgeWidth, m_height - 2 * m_mirroredEdgeWidth);

	*this = Copy(rect);
}

NImage NImage::RemovedEdges() const
{
	NImage temp(*this);

	temp.RemoveEdges();

	return temp;
}

void NImage::InsertImage(const NImage& from, NImage& to, int x, int y)
{
	for (int i = 0; i < from.Height(); i++)
	{
		for (int j = 0; j < from.Width(); j++)
		{
			to.Bits()[x + (y + i) * to.Width() + j] = from.Bits()[i * from.Width() + j];
		}
	}
}

double NImage::ApplyMaskWithMultiplicationAt(int row, int col, const Mask& mask)
{
	int radius = mask.GetRadius();
	double accum = 0.f;
	const double* maskRow = nullptr;
	const double* imgRow = nullptr;

	for (int i = row - radius, mi = 0; i <= row + radius; i++, mi++)
	{
		maskRow = mask.Ptr(mi);
		imgRow = Ptr(i);

		for (int j = col - radius, mj = 0; j <= col + radius; j++, mj++)
		{
			accum += maskRow[mj] * imgRow[j];
		}
	}

	return accum;
}

void NImage::GetImageData(vtkSmartPointer<vtkImageData> data)
{
	unsigned char* values = static_cast<unsigned char*>(data->GetScalarPointer());
	Mirror(false, true);

	int scalar = data->GetNumberOfScalarComponents();

	for (int i = 0, iter = 0; i < GetNumOfPixels(); i++, iter+=scalar)
	{
		if (m_bits[i] > 1.) m_bits[i] = 1.;
		if (m_bits[i] < 0.) m_bits[i] = 0.;
		unsigned char temp(static_cast<unsigned char>(m_bits[i] * 255.));

		if (scalar <= 3)
		{
			for (int s = 0; s < scalar; s++)
			{
				values[iter + s] = temp;
			}
		}
		else if (scalar == 4)
		{
			for (int s = 0; s < 3; s++)
			{
				values[iter + s] = temp;
			}
			values[iter + 3] = 255;
		}
			
	}

	Mirror(false, true);
}

double NImage::ApplyMaskWithMultiplicationAt(int row, int col, const NonUniformMask& mask)
{
	int xRadius = mask.GetXRadius();
	int yRadius = mask.GetYRadius();
	double accum = 0.;
	const double* maskRow = nullptr;
	const double* imgRow = nullptr;

	for (int i = row - yRadius, mi = 0; i <= row + yRadius; i++, mi++)
	{
		maskRow = mask.Ptr(mi);
		imgRow = Ptr(i);

		for (int j = col - xRadius, mj = 0; j <= col + xRadius; j++, mj++)
		{
			accum += maskRow[mj] * imgRow[j];
		}
	}

	return accum;
}

void NImage::ApplyMaskWithValuesExtractionAt(int row, int col, Mask& mask)
{
	int radius = mask.GetRadius();
	double* maskRow = nullptr;
	const double* imgRow = nullptr;

	for (int i = row - radius, mi = 0; i <= row + radius; i++, mi++)
	{
		maskRow = mask.Ptr(mi);
		imgRow = Ptr(i);

		for (int j = col - radius, mj = 0; j <= col + radius; j++, mj++)
		{
			if (maskRow[mj] == 1.)
			{
				maskRow[mj] = imgRow[j];
			}
		}
	}
}

void NImage::SetAllValuesTo(double val)
{
	for (int i = 0; i < GetNumOfPixels(); i++)
	{
		m_bits[i] = val;
	}
}

int NImage::GetMirrorEdgeWidth() const
{
	return m_mirroredEdgeWidth;
}

int NImage::GetUnMirroredRowStart() const
{
	return m_mirroredEdgeWidth;
}

int NImage::GetUnMirroredColumnStart() const
{
	return m_mirroredEdgeWidth;
}

int NImage::GetUnMirroredRowEnd() const
{
	return m_height - m_mirroredEdgeWidth - 1;
}

int NImage::GetUnMirroredColumnEnd() const
{
	return m_width - m_mirroredEdgeWidth - 1;
}