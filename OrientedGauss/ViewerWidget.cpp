#include "ViewerWidget.h"

ViewerWidget::ViewerWidget(QWidget* parent, QVTKOpenGLNativeWidget *widget, QMainWindow *window)
	: QWidget(parent), ui(new Ui::ViewerWidget)
{
	ui->setupUi(this);

	ui->tabWidget->setTabEnabled(1, false);
	ui->tabWidget->setTabEnabled(2, false);

	m_window = window;
	m_glWidget = widget;	
	m_glWidget->GetRenderWindow()->SetNumberOfLayers(2);

	for (int i = 0; i < 3; i++)
	{		
		m_infoRenderers[i] = vtkSmartPointer<vtkOpenGLRenderer>::New();
		m_infoRenderers[i]->SetLayer(1);
	}	

	m_imageViewer = vtkSmartPointer<vtkImageViewer2>::New();
	m_imageViewer->GetRenderer()->SetBackground(1, 1, 1);
	m_imageViewer->GetRenderer()->SetLayer(0);

	m_glWidget->GetRenderWindow()->AddRenderer(m_infoRenderers[0]);
	m_imageViewer->SetRenderWindow(m_glWidget->GetRenderWindow());

	m_style = vtkSmartPointer<VtkCustomInteractorStyle>::New();
	m_style->SetInteractionModeToImage2D();
	m_style->SetImageViewer(m_imageViewer);
	m_style->SetInfoRenderer(m_infoRenderers[0]);
	m_style->InitInfoRenderer();
	m_style->SetRenderWindow(m_glWidget->GetRenderWindow());
	m_style->SetDialog(&m_newImageDialog);

	m_interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	m_interactor->SetInteractorStyle(m_style);
	m_style->SetInteractor(m_interactor);

	m_glWidget->GetRenderWindow()->SetInteractor(m_interactor);
	m_interactor->Initialize();	

	connect(&m_newImageDialog, SIGNAL(AcceptedSelection(NewImageData)), window, SLOT(CreateNewTab(NewImageData)));
	connect(&m_newImageDialog, SIGNAL(Start()), window, SLOT(DisableCreation()));
	connect(&m_newImageDialog, SIGNAL(Done()), window, SLOT(EnableCreation()));
	m_newImageDialog.setModal(true);

	m_originalImageData = vtkSmartPointer<vtkImageData>::New();
	m_resultImageData = vtkSmartPointer<vtkImageData>::New();


	m_callback = vtkSmartPointer<VtkInteractionCallback>::New();
	m_style->AddObserver(m_style->MouseClick, m_callback);
}

ViewerWidget::~ViewerWidget()
{
	CleanUpForNextWidget();
}

/*********************************/
//
//
//		PUBLIC METHODS
//
/*********************************/

void ViewerWidget::SelectionOn()
{
	m_style->SelectionOn();

	NonUniformMask mask;

	mask.CreateMask(1., 0.05, 90. * M_PI / 180.);
	mask.Print();
}

void ViewerWidget::EllipseOn()
{
	m_style->EllipseOn();
}

void ViewerWidget::EllipseOff()
{
	m_style->EllipseOff();
}

void ViewerWidget::RemoveEllipses()
{
	m_infoRenderers[GetCurrentTabIndex()]->RemoveAllViewProps();
	m_infoRenderers[GetCurrentTabIndex()]->AddViewProp(m_cornerAnnotations[GetCurrentTabIndex()]);
	m_style->InitInfoRenderer();
	m_interactor->Render();
}

void ViewerWidget::CleanUpForNextWidget()
{
	m_glWidget->GetRenderWindow()->RemoveRenderer(m_imageViewer->GetRenderer());
	m_glWidget->GetRenderWindow()->RemoveRenderer(m_infoRenderers[GetCurrentTabIndex()]);
}

void ViewerWidget::TakeOverRenderWindow()
{
	m_glWidget->GetRenderWindow()->AddRenderer(m_imageViewer->GetRenderer());
	m_glWidget->GetRenderWindow()->AddRenderer(m_infoRenderers[GetCurrentTabIndex()]);
	m_glWidget->GetRenderWindow()->SetInteractor(m_interactor);
	m_interactor->Render();
}

void ViewerWidget::SetOriginalImage(const QString &fileName)
{
	vtkSmartPointer<vtkImageReader2Factory> readerFactory = vtkSmartPointer<vtkImageReader2Factory>::New();
	vtkSmartPointer<vtkImageReader2> reader = readerFactory->CreateImageReader2(fileName.toStdString().c_str());

	if (reader == nullptr)
	{
		std::cerr << "Unable to read image\n";
		return;
	}

	reader->SetFileName(fileName.split('/').back().toStdString().c_str());
	reader->Update();

	vtkSmartPointer<vtkImageCast> originalCast = vtkSmartPointer<vtkImageCast>::New();
	originalCast->SetInputData(reader->GetOutput());
	originalCast->SetOutputScalarTypeToUnsignedChar();
	originalCast->Update();

	m_imageViewer->SetInputData(originalCast->GetOutput());
	m_imageViewer->GetImageActor()->GetMapper()->BorderOn();
	m_imageViewer->GetImageActor()->InterpolateOff();
	m_imageViewer->GetImageActor()->SetPosition(0.5, 0.5, 0.);
	m_imageViewer->GetRenderer()->ResetCamera();
	m_imageViewer->GetRenderer()->GetActiveCamera()->SetParallelProjection(true);
	m_infoRenderers[0]->SetActiveCamera(m_imageViewer->GetRenderer()->GetActiveCamera());
	m_imageViewer->GetImageActor()->Update();

	for (int i = 0; i < 3; i++)
	{
		m_cornerAnnotations[i] = vtkSmartPointer<vtkCornerAnnotation>::New();

		m_cornerAnnotations[i]->SetLinearFontScaleFactor(2);
		m_cornerAnnotations[i]->SetNonlinearFontScaleFactor(1);
		m_cornerAnnotations[i]->SetMaximumFontSize(15);
		m_cornerAnnotations[i]->SetText(i, "");
		m_cornerAnnotations[i]->GetTextProperty()->SetColor(1, 0, 0);
		m_infoRenderers[i]->AddViewProp(m_cornerAnnotations[i]);
	}	

	m_style->SetImageRenderer(m_imageViewer->GetRenderer());
	m_style->SetCornerAnnotation(m_cornerAnnotations[0]);
	m_style->SetOriginalSource(reader->GetOutput());

	m_originalNImage = NImage(reader->GetOutput());
	m_originalImageData->DeepCopy(reader->GetOutput());

	m_callback->SetRenderer(m_infoRenderers[0]);
}

void ViewerWidget::SetOriginalImage(vtkSmartPointer<vtkImageData> imageData)
{
	int ext[6];
	imageData->GetExtent(ext);

	m_imageViewer->SetInputData(imageData);

	m_imageViewer->GetImageActor()->GetMapper()->BorderOn();
	m_imageViewer->GetImageActor()->InterpolateOff();
	m_imageViewer->GetImageActor()->SetPosition(-ext[0] + 0.5, -ext[2] + 0.5, 0.);
	m_imageViewer->GetRenderer()->ResetCamera();
	m_imageViewer->GetRenderer()->GetActiveCamera()->SetParallelProjection(true);
	m_infoRenderers[0]->SetActiveCamera(m_imageViewer->GetRenderer()->GetActiveCamera());

	for (int i = 0; i < 3; i++)
	{
		m_cornerAnnotations[i] = vtkSmartPointer<vtkCornerAnnotation>::New();

		m_cornerAnnotations[i]->SetLinearFontScaleFactor(2);
		m_cornerAnnotations[i]->SetNonlinearFontScaleFactor(1);
		m_cornerAnnotations[i]->SetMaximumFontSize(15);
		m_cornerAnnotations[i]->SetText(i, "");
		m_cornerAnnotations[i]->GetTextProperty()->SetColor(1, 0, 0);
		m_infoRenderers[i]->AddViewProp(m_cornerAnnotations[i]);
	}

	m_style->SetImageRenderer(m_imageViewer->GetRenderer());
	m_style->SetCornerAnnotation(m_cornerAnnotations[0]);
	m_style->SetOriginalSource(imageData);

	m_originalNImage = NImage(imageData);
	m_originalImageData->DeepCopy(imageData);
	m_callback->SetRenderer(m_infoRenderers[0]);
}

void ViewerWidget::LHE(const OperationLHEImplicitParams &params) const
{
	OperationLHEImplicit operation;

	operation.Do(params);
}

void ViewerWidget::Gaussian(int numIters)
{
	int width = m_originalNImage.Width();
	int height = m_originalNImage.Height();
	NImage lheResult = m_originalNImage;
	NImage workingCopy = m_originalNImage;
	NImage iterBuff = workingCopy;

	if (width < 9 || height < 9)
	{
		std::cout << "Image dimensions are too small\n";
		return;
	}	

	int threadsNum = 1;

	for (int iter = 0; iter < numIters; ++iter)
	{
		OperationLHEImplicitParams params;

		params.from = &workingCopy;
		params.to = &lheResult;
		params.numOfSteps = 3;
		params.omega = 1.1;
		params.timeStep = 0.25;
		params.tolerance = 0.001;

		LHE(params);
		lheResult.ComputeGradients();

		m_gaussianData.resize(m_originalNImage.GetNumOfPixels());

		NImage buff = params.from->MirroredEdges(9);
		m_callback->SetWidth(width);
		m_callback->SetHeight(height);

#pragma omp parallel for
		for (int j = 0; j < m_originalNImage.Height(); j++)
		{
			double *vals = iterBuff.Ptr(j);

			for (int i = 0; i < m_originalNImage.Width(); i++)
			{
				Gradient grad = lheResult.GetGradientAt(i, j);
				int index = j * width + i;

				m_gaussianData[index].rX = 1.;
				m_gaussianData[index].rY = gPM(lheResult.GetGradientNormAt(i, j), K);

				if (m_gaussianData[index].rY > 0.8) m_gaussianData[index].rY = 1.;
				else if (m_gaussianData[index].rY <= 0.05) m_gaussianData[index].rY = 0.05;

				NonUniformMask mask;
				double angle;

				if (grad.ux == 0 && grad.uy == 0) angle = M_PI / 2.;
				else if (grad.uy != 0) angle = atan(grad.ux / grad.uy);
				else angle = 0.;

				mask.CreateMask(m_gaussianData[index].rX, m_gaussianData[index].rY, angle);

				m_gaussianData[index].alpha = mask.GetAngle();

				vals[i] = buff.ApplyMaskWithMultiplicationAt(j + 9, i + 9, mask);
			}

			threadsNum = omp_get_num_threads();
			int tid = omp_get_thread_num();

			double total = 1. / ((static_cast<double>(height) / threadsNum) / 100.);

			if (tid == 0)
			{
				std::cout << "Done " << total * (j + 1) << "%" << std::endl;
			}
		}

		workingCopy = iterBuff;
		std::cout << "Finished iter: " << iter + 1 << std::endl;
	}

	m_resultImage = iterBuff;

	m_resultImageData->DeepCopy(m_imageViewer->GetImageActor()->GetInput());
	m_resultImage.GetImageData(m_resultImageData);

	std::cout << "Finished!\n";
	std::cout << "Computation was run on " << threadsNum << " threads\n";

	ShowResult(m_resultImageData);
}

void ViewerWidget::Tensor(int numIters)
{
	int width = m_originalNImage.Width();
	int height = m_originalNImage.Height();

	NImage source;
	NImage result = m_originalNImage;
	int maskLimit = 15;
	
	std::vector<std::vector<NonUniformMask>> masks;
	masks.resize(height);

	for (int i = 0; i < height; ++i)
	{
		masks[i].resize(width);
	}

	int recompute = 5;

	NImage aCoefs;
	NImage bCoefs;
	NImage cCoefs;

	for (int iter = 0; iter < numIters; ++iter)
	{	
		source = result;

		if (iter % recompute == 0)
		{		
			NImage lheResult = source;

			if (width < maskLimit || height < maskLimit)
			{
				std::cout << "Image dimensions are too small\n";
				return;
			}

			OperationLHEImplicitParams params = m_sourceParams;

			params.to = &lheResult;
			params.from = &source;

			LHE(params);
			lheResult.ComputeGradientsScaled();

			aCoefs = NImage(height, width);
			bCoefs = NImage(height, width);
			cCoefs = NImage(height, width);

			for (int j = 0; j < height; j++)
			{
				double *aVals = aCoefs.Ptr(j);
				double *bVals = bCoefs.Ptr(j);
				double *cVals = cCoefs.Ptr(j);

				for (int i = 0; i < width; i++)
				{
					Gradient grad = lheResult.GetGradientAt(i, j);

					aVals[i] = grad.ux * grad.ux;
					bVals[i] = grad.ux * grad.uy;
					cVals[i] = grad.uy * grad.uy;
				};
			}

			params = m_aParams;
			params.to = &aCoefs;
			params.from = &aCoefs;
			LHE(params);
		}
		
		NImage buff = source.MirroredEdges(maskLimit);
			
		m_resultImage = source;
		m_gaussianData.resize(source.GetNumOfPixels());

		m_callback->SetWidth(width);
		m_callback->SetHeight(height);
		int threadsNum;

#pragma omp parallel for
		for (int j = 0; j < source.Height(); j++)
		{
			double *vals = m_resultImage.Ptr(j);
			double *aVals = aCoefs.Ptr(j);
			double *bVals = bCoefs.Ptr(j);
			double *cVals = cCoefs.Ptr(j);		

			for (int i = 0; i < source.Width(); i++)
			{			
				int index = j * width + i;

				if (iter % recompute == 0)
				{
					double mi1 = 0.5 * (aVals[i] + cVals[i] + sqrt((aVals[i] - cVals[i]) * (aVals[i] - cVals[i]) + 4 * bVals[i] * bVals[i]));
					double mi2 = 0.5 * (aVals[i] + cVals[i] - sqrt((aVals[i] - cVals[i]) * (aVals[i] - cVals[i]) + 4 * bVals[i] * bVals[i]));

					double w1 = -1 * (cVals[i] - aVals[i] + sqrt((aVals[i] - cVals[i]) * (aVals[i] - cVals[i]) + 4 * bVals[i] * bVals[i]));
					double w2 = 2 * bVals[i];
					double vLength = sqrt(w1 * w1 + w2 * w2);

					w1 /= vLength;
					w2 /= vLength;

					double angle = -atan(w2 / w1);

					if (mi1 == mi2)
					{
						m_gaussianData[index].rX = m_lambda;
						m_gaussianData[index].rY = m_lambda;
					}
					else
					{
						m_gaussianData[index].rX = m_lambda + (1 - m_lambda)*exp(-m_cParam / ((mi1 - mi2) * (mi1 - mi2)));
						m_gaussianData[index].rY = m_lambda;
					}
					
					masks[j][i].CreateMask(m_gaussianData[index].rX, m_gaussianData[index].rY, angle);				
					
					m_gaussianData[index].alpha = masks[j][i].GetAngle();
					m_gaussianData[index].a = aVals[i];
					m_gaussianData[index].b = bVals[i];
					m_gaussianData[index].c = cVals[i];
					m_gaussianData[index].eigenVal1 = mi1;
					m_gaussianData[index].eigenVal2 = mi2;				
				}

				vals[i] = buff.ApplyMaskWithMultiplicationAt(j + maskLimit, i + maskLimit, masks[j][i]);
			}			

			int tid = omp_get_thread_num();
			threadsNum = omp_get_num_threads();			

			double total = 1. / ((static_cast<double>(height) / threadsNum) / 100.);

			if (tid == 0)
			{
				std::cout << "Done " << total * (j + 1) << "%" << std::endl;
			}
		}

		result = m_resultImage;

		std::cout << "Finished!\n";
		std::cout << "Computation was run on " << threadsNum << " threads\n";
	}


	m_resultImageData->DeepCopy(m_imageViewer->GetImageActor()->GetInput());
	m_resultImage.GetImageData(m_resultImageData);
	ShowResult(m_resultImageData);
}

void ViewerWidget::LHECustom(const OperationLHEImplicitParams &params, int numIters)
{
	int width = m_originalNImage.Width();
	int height = m_originalNImage.Height();

	if (width < 9 || height < 9)
	{
		std::cout << "Image dimensions are too small\n";
		return;
	}

	NImage workingCopy = m_originalNImage;

	for (int i = 0; i < numIters; ++i)
	{
		OperationLHEImplicitParams paramsLoc = params;
		paramsLoc.from = &workingCopy;
		paramsLoc.to = &m_resultImage;

		LHE(paramsLoc);

		workingCopy = m_resultImage;
	}

	m_resultImageData->DeepCopy(m_imageViewer->GetImageActor()->GetInput());
	m_resultImage.GetImageData(m_resultImageData);
	ShowResult(m_resultImageData);
}

void ViewerWidget::SetEllipseScale(double scale)
{
	m_callback->SetScale(scale);
}

void ViewerWidget::ShowResult(vtkSmartPointer<vtkImageData> imageData)
{
	m_callback->SetRenderer(m_infoRenderers[1]);
	m_callback->SetGaussData(&m_gaussianData);
	ui->tabWidget->setTabEnabled(1, true);
	ui->tabWidget->setTabEnabled(2, true);

	m_style->SetInfoRenderer(m_infoRenderers[2]);
	m_style->InitInfoRenderer();

	m_style->SetInfoRenderer(m_infoRenderers[1]);
	m_style->InitInfoRenderer();
	m_style->SetInfoRenderer(m_infoRenderers[m_lastTabIndex]);
	ui->tabWidget->setCurrentIndex(1);
}

/*********************************/
//
//
//		PRIVATE METHODS
//
/*********************************/


/*********************************/
//
//
//		PUBLIC SLOTS
//
/*********************************/

void ViewerWidget::SwitchTabs(int index)
{
	ui->tabWidget->setCurrentIndex(index);
}

/*********************************/
//
//
//		PRIVATE SLOTS
//
/*********************************/

void ViewerWidget::on_tabWidget_currentChanged(int index)
{	
	vtkSmartPointer<vtkImageData> imageData;

	m_callback->SetRenderer(m_infoRenderers[index]);

	if (index == 0)
	{
		imageData = m_originalImageData;
	}
	else if (index == 1)
	{
		imageData = m_resultImageData;
	}
	else if (index == 2)
	{
		vtkSmartPointer<vtkImageAppend> append = vtkSmartPointer<vtkImageAppend>::New();
		append->AddInputData(m_originalImageData);
		append->AddInputData(m_resultImageData);
		append->Update();
		imageData = append->GetOutput();

		m_callback->SetRenderer(nullptr);
	}

	int ext[6];
	imageData->GetExtent(ext);	
	m_imageViewer->SetInputData(imageData);
	m_imageViewer->GetImageActor()->GetMapper()->BorderOn();
	m_imageViewer->GetImageActor()->InterpolateOff();
	m_imageViewer->GetImageActor()->SetPosition(-ext[0] + 0.5, -ext[2] + 0.5, 0.);
	m_imageViewer->GetImageActor()->Update();
	m_imageViewer->GetRenderer()->ResetCamera();
	m_imageViewer->GetRenderer()->GetActiveCamera()->SetParallelProjection(true);
	m_infoRenderers[index]->SetActiveCamera(m_imageViewer->GetRenderer()->GetActiveCamera());
	

	m_glWidget->GetRenderWindow()->RemoveRenderer(m_infoRenderers[m_lastTabIndex]);
	m_glWidget->GetRenderWindow()->AddRenderer(m_infoRenderers[index]);
	
	m_style->SetInfoRenderer(m_infoRenderers[index]);
	m_style->SetCornerAnnotation(m_cornerAnnotations[index]);	
	m_style->SetOriginalSource(imageData);
	

	m_interactor->Render();

	m_lastTabIndex = index;

	emit TabChanged(index);
}

void ViewerWidget::ExportImageToVtk(const std::string &path)
{
	if (m_resultImage.Width() == 0 && m_resultImage.Height() == 0)
	{
		return;
	}

	std::ofstream file(path);
	file << "# vtk DataFile Version 2.0" << std::endl << "Image result" << std::endl << "ASCII" << std::endl << "DATASET POLYDATA" << std::endl;
	file << "POINTS " << (m_resultImage.Width() + 1) * (m_resultImage.Height() + 1) << " int" << std::endl;

	for (int j = 0; j < m_resultImage.Height() + 1; ++j)
	{
		for (int i = 0; i < m_resultImage.Width() + 1; ++i)
		{
			file << i << " " << j << " " << 0 << std::endl;
		}
	}

	file << "POLYGONS " << m_resultImage.Height() * m_resultImage.Width() << " " << 5 * m_resultImage.Height() * m_resultImage.Width() << std::endl;

	for (int j = 0; j < m_resultImage.Height(); ++j)
	{
		for (int i = 0; i < m_resultImage.Width(); ++i)
		{
			int current = i + j / m_resultImage.Width() + (j % m_resultImage.Height()) * (m_resultImage.Width() + 1);

			file << 4 << " " << current << " " << current + 1 << " " << current + m_resultImage.Width() + 2 << " " << current + m_resultImage.Width() + 1 << std::endl;
		}
	}

	file << "CELL_DATA " << m_resultImage.Height() * m_resultImage.Width() << std::endl;
	file << "SCALARS " << "original_values int 1" << std::endl;
	file << "LOOKUP_TABLE default" << std::endl;

	for (int j = 0; j < m_resultImage.Height(); ++j)
	{
		for (int i = 0; i < m_resultImage.Width(); ++i)
		{					
			file << int(m_resultImage[j][i] * 255) << std::endl;
		}
	}

	file << "SCALARS " << "eigenvalues_diff float 1" << std::endl;
	file << "LOOKUP_TABLE default" << std::endl;

	file << std::fixed;
	file << std::setprecision(4);

	for (int j = 0; j < m_resultImage.Height(); ++j)
	{
		for (int i = 0; i < m_resultImage.Width(); ++i)
		{
			int index = j * m_resultImage.Width() + i;
			file << m_gaussianData[index].eigenVal1 - m_gaussianData[index].eigenVal2 << std::endl;
		}
	}

	file.close();
}

void ViewerWidget::ExportImageToPGM(const std::string &path)
{
	if (m_resultImage.Width() == 0 && m_resultImage.Height() == 0)
	{
		return;
	}

	std::ofstream out(path);

	if (!out.is_open()) return;

	out << "P2" << std::endl;
	out << m_resultImage.Width() << " " << m_resultImage.Height() << std::endl;
	out << 255 << std::endl;

	for (int j = 0; j < m_resultImage.Height(); ++j)
	{
		for (int i = 0; i < m_resultImage.Width(); ++i)
		{
			out << static_cast<int>(m_resultImage[j][i] * 255) << " ";
		}

		out << std::endl;
	}

	out.close();
}