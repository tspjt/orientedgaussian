#pragma once

#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <QString>

class NImage;

#define MY_PI 3.14159265359

struct NewImageData
{
	vtkSmartPointer<vtkImageData> data;
	QString imageName;
};

struct Gradient
{
	double ux;
	double uy;
};

struct GaussianData
{
	double rX;
	double rY;
	double alpha;
	double a;
	double b;
	double c;
	double eigenVal1;
	double eigenVal2;
	double vec1[2];
	double vec2[2];
};

struct OperationLHEImplicitParams
{
	NImage* from = nullptr;
	NImage* to = nullptr;
	size_t numOfSteps = 3;
	double timeStep = 0.25;
	double tolerance = 1e-3;
	double omega = 1.;	
};