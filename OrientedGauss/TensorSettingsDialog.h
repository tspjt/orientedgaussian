#pragma once

#include <qdialog.h>
#include "ui_TensorSettingsDialog.h"
#include "GeneralTypes.h"

class TensorSettingsDialog : public QDialog
{
	Q_OBJECT

public:
	TensorSettingsDialog(QWidget* parent = Q_NULLPTR);
	bool Confirmed() const { return m_bConfirmed; }
	double GetCParam() const { return m_cParam; }
	double GetLambda() const { return m_lambda; }
	int GetIterationCount() const { return m_iterationCount; }
	OperationLHEImplicitParams GetOriginalLHEParams() const { return m_originalParams; }
	OperationLHEImplicitParams GetACoefLHEParams() const { return m_aCoef; }
	OperationLHEImplicitParams GetBCoefLHEParams() const { return m_bCoef; }
	OperationLHEImplicitParams GetCCoefLHEParams() const { return m_cCoef; }

private:
	Ui::TensorSettingsDialog* ui;
	bool m_bConfirmed = false;

	OperationLHEImplicitParams m_originalParams;
	OperationLHEImplicitParams m_aCoef;
	OperationLHEImplicitParams m_bCoef;
	OperationLHEImplicitParams m_cCoef;
	double m_lambda;
	double m_cParam;
	int m_iterationCount;

private slots:
	void on_pushButtonOK_pressed();
};
