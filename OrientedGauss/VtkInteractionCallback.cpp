#include "VtkInteractionCallback.h"

vtkStandardNewMacro(VtkInteractionCallback);

VtkInteractionCallback::VtkInteractionCallback()
{
	
}

VtkInteractionCallback::~VtkInteractionCallback()
{

}

void VtkInteractionCallback::Execute(vtkObject *, unsigned long vtkNotUsed(event), void *callData)
{
	if (m_renderer == nullptr) return;

	int* coords = static_cast<int*>(callData);

	if (coords[0] < 0 || coords[1] < 0 || coords[0] >= m_width || coords[1] >= m_height) return;

	GaussianData gaussData = (*m_gaussData)[((m_height - 1) - coords[1]) * m_width + coords[0]];
	NonUniformMask mask;

	std::cout << std::setprecision(3);

	mask.CreateMask(gaussData.rX, gaussData.rY, gaussData.alpha);

	mask.Print();
	std::cout << "sigmaX: " << gaussData.rX << " sigmaY: " << gaussData.rY << " alpha degrees: " << gaussData.alpha * 180. / M_PI << std::endl;
	std::cout << "a: " << gaussData.a << " b: " << gaussData.b << " c: " << gaussData.c << " eigenVal1: " << gaussData.eigenVal1 << " eigenVal2: " << gaussData.eigenVal2 << std::endl;

	vtkSmartPointer<vtkEllipseArcSource> ellipse = vtkSmartPointer<vtkEllipseArcSource>::New();
	ellipse->SetCenter(coords[0] + 0.5, coords[1] + 0.5, 0);
	ellipse->SetSegmentAngle(360);
	ellipse->SetMajorRadiusVector(m_scale * cos(gaussData.alpha) * gaussData.rX * 2, m_scale * sin(gaussData.alpha) * gaussData.rX * 2, 0);
	ellipse->SetRatio(gaussData.rY / gaussData.rX);	

	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(ellipse->GetOutputPort());

	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);

	double mi1 = fabs(gaussData.eigenVal1);
	double mi2 = fabs(gaussData.eigenVal2);

	if (mi1 >= EIGEN_ZERO_TOL + mi2 && mi2 <= EIGEN_ZERO_TOL)		//hrana
	{
		actor->GetProperty()->SetColor(0, 1, 0);
	}
	else if (mi1 <= EIGEN_ZERO_TOL && mi2 <= EIGEN_ZERO_TOL)	//homogenna
	{
		actor->GetProperty()->SetColor(1, 0, 0);
	}
	else if (mi1 >= mi2 && mi2 >= EIGEN_ZERO_TOL)	//roh
	{
		actor->GetProperty()->SetColor(1, 1, 0);
	}
	else
		actor->GetProperty()->SetColor(1, 1, 1);
	

	m_renderer->AddActor(actor);
}