#pragma once

#include <qdialog.h>
#include "ui_NewImageDialog.h"
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <QVTKOpenGLNativeWidget.h>
#include "GeneralTypes.h"

class NewImageDialog : public QDialog
{
	Q_OBJECT

public:
	NewImageDialog(QWidget* parent = Q_NULLPTR);

	void SetImageData(vtkSmartPointer<vtkImageData> data) { m_imageData.data = data; }
	void Exec();

private:
	Ui::NewImageDialog* ui;
	NewImageData m_imageData;

private slots:
	void on_pushButtonOK_pressed();
	void on_pushButtonCancel_pressed();

signals:
	void AcceptedSelection(NewImageData);
	void Start();
	void Done();
};