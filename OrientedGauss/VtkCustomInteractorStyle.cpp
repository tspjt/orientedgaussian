#include "VtkCustomInteractorStyle.h"

vtkStandardNewMacro(VtkCustomInteractorStyle);

VtkCustomInteractorStyle::VtkCustomInteractorStyle()
{
	vtkSmartPointer<vtkPoints> m_rectangleSelectionPoints = vtkSmartPointer<vtkPoints>::New();
	vtkSmartPointer<vtkCellArray> m_rectangleCellArray = vtkSmartPointer<vtkCellArray>::New();
	m_rectangle = vtkSmartPointer<vtkPolyData>::New();

	m_rectangleSelectionPoints->SetNumberOfPoints(4);
	m_rectangleSelectionPoints->SetPoint(0, 0., 0., 0.);
	m_rectangleSelectionPoints->SetPoint(1, 0., 0., 0.);
	m_rectangleSelectionPoints->SetPoint(2, 0., 0., 0.);
	m_rectangleSelectionPoints->SetPoint(3, 0., 0., 0.);

	m_rectangleCellArray->InsertNextCell(5);
	m_rectangleCellArray->InsertCellPoint(0);
	m_rectangleCellArray->InsertCellPoint(1);
	m_rectangleCellArray->InsertCellPoint(2);
	m_rectangleCellArray->InsertCellPoint(3);
	m_rectangleCellArray->InsertCellPoint(0);

	m_rectangle->SetPoints(m_rectangleSelectionPoints);
	m_rectangle->SetLines(m_rectangleCellArray);
	MouseClick = vtkCommand::UserEvent + 1;
}

VtkCustomInteractorStyle::~VtkCustomInteractorStyle()
{

}

void VtkCustomInteractorStyle::SortSelectionRectangle(double &xStart, double &yStart, double &xEnd, double &yEnd) const
{
	double buff = xEnd;
	
	if (xStart > xEnd)
	{
		xEnd = xStart;
		xStart = buff;
	}

	buff = yEnd;

	if (yStart > yEnd)
	{
		yEnd = yStart;
		yStart = buff;
	}
}

void VtkCustomInteractorStyle::ClipSelectionToImage(double &m_xStart, double &m_yStart, double &m_xEnd, double &m_yEnd, double width, double height) const
{
	if (m_xStart < 0.)
	{
		m_xStart = 0.;
	}

	if (m_yStart < 0)
	{
		m_yStart = 0.;
	}

	if (m_xEnd > width)
	{
		m_xEnd = width - 1;
	}

	if (m_yEnd > height)
	{
		m_yEnd = height - 1;
	}
}

void VtkCustomInteractorStyle::OnLeftButtonDown()
{
	m_isLeftMouseButtonPressed = true;
	double coords[2];
	int coordsInt[2];

	GetCursorPosition(coords, coordsInt);	

	if (m_ellipseOn) InvokeEvent(MouseClick, &coordsInt);

	if (m_selectionOn)
	{			
		m_xStart = coords[0];
		m_yStart = coords[1];	

		vtkSmartPointer<vtkPoints> points = m_rectangle->GetPoints();

		points->SetPoint(0, m_xStart, m_yStart, 0.);
		points->SetPoint(1, m_xStart, m_yStart, 0.);
		points->SetPoint(2, m_xStart, m_yStart, 0.);
		points->SetPoint(3, m_xStart, m_yStart, 0.);
	}	
}

bool VtkCustomInteractorStyle::IsSelectionViable(double width, double height) const
{
	bool ok = true;

	if (m_xStart < 0 || m_xStart >= width) ok = false;
	if (m_xEnd < 0 || m_xEnd >= width) ok = false;
	if (m_yStart < 0 || m_yStart >= height) ok = false;
	if (m_yEnd < 0 || m_yEnd >= height) ok = false;

	return ok;
}

void VtkCustomInteractorStyle::OnLeftButtonUp()
{
	m_isLeftMouseButtonPressed = false;

	if (m_selectionOn)
	{		
		m_selectionOn = false;
		double coords[2];
		int imageDims[3];

		GetCursorPosition(coords);

		m_xEnd = coords[0];
		m_yEnd = coords[1];

		vtkSmartPointer<vtkExtractVOI> imagePart = vtkSmartPointer<vtkExtractVOI>::New();
		imagePart->SetInputData(m_originalSource);

		m_imageViewer->GetInput()->GetDimensions(imageDims);

		int ext[6];
		m_originalSource->GetExtent(ext);

		SortSelectionRectangle(m_xStart, m_yStart, m_xEnd, m_yEnd);
		ClipSelectionToImage(m_xStart , m_yStart, m_xEnd, m_yEnd, imageDims[0], imageDims[1]);

		if (!IsSelectionViable(imageDims[0], imageDims[1]))
		{
			ResetSelection();
			return;
		}

		imagePart->SetVOI(m_xStart + ext[0], m_xEnd + ext[0], m_yStart + ext[2], m_yEnd + ext[2], 0, 0);
		imagePart->Update();
		m_newImageDialog->SetImageData(imagePart->GetOutput());

		m_newImageDialog->Exec();
	}	

	ResetSelection();
}

void VtkCustomInteractorStyle::OnMouseMove()
{
	vtkInteractorStyleImage::OnMouseMove();

	double coords[2];

	GetCursorPosition(coords);

	if (m_isLeftMouseButtonPressed && m_selectionOn)
	{
		vtkSmartPointer<vtkPoints> points = m_rectangle->GetPoints();

		points->SetPoint(1, coords[0], m_yStart, 0.);
		points->SetPoint(2, coords[0], coords[1], 0.);
		points->SetPoint(3, m_xStart, coords[1], 0.);

		points->Modified();
	}

	m_interactor->Render();
}

void VtkCustomInteractorStyle::OnKeyDown()
{
	std::string key = this->Interactor->GetKeySym();

	if ("Escape" == key)
	{
		ResetSelection();
	}
}

void VtkCustomInteractorStyle::ResetSelection()
{
	vtkSmartPointer<vtkPoints> points = m_rectangle->GetPoints();

	points->SetPoint(0, 0., 0., 0.);
	points->SetPoint(1, 0., 0., 0.);
	points->SetPoint(2, 0., 0., 0.);
	points->SetPoint(3, 0., 0., 0.);

	points->Modified();
	m_rectangle->Modified();

	m_renderWindow->Render();
}

void VtkCustomInteractorStyle::SetInfoRenderer(vtkSmartPointer<vtkRenderer> renderer)
{
	m_infoRenderer = renderer;
}

void VtkCustomInteractorStyle::InitInfoRenderer()
{
	m_polygonMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	m_polygonMapper->SetInputData(m_rectangle);
	m_polygonMapper->Update();

	vtkSmartPointer<vtkActor> rectangleSelection = vtkSmartPointer<vtkActor>::New();
	rectangleSelection->SetMapper(m_polygonMapper);
	rectangleSelection->GetProperty()->SetColor(1, 0, 0);

	m_infoRenderer->AddActor(rectangleSelection);
}

void VtkCustomInteractorStyle::GetCursorPosition(double coords[2], int* coordsInt)
{
	vtkSmartPointer<vtkCoordinate> coordinate = vtkSmartPointer<vtkCoordinate>::New();
	coordinate->SetCoordinateSystemToDisplay();

	coordinate->SetValue(m_interactor->GetEventPosition()[0], m_interactor->GetEventPosition()[1], 0.0);
	double *position = coordinate->GetComputedWorldValue(m_infoRenderer);

	coords[0] = position[0];
	coords[1] = position[1];

	int x, y;

	if (coords[0] < 0)
	{
		x = floor(position[0]);
	}
	else
	{
		x = static_cast<int>(position[0]);
	}
	
	if (coords[1] < 0)
	{
		y = floor(position[1]);
	}
	else
	{
		y = static_cast<int>(position[1]);
	}

	std::stringstream ss;
	ss << "x: " << x << " y: " << y;

	m_cornerAnnotation->SetText(0, ss.str().c_str());

	if (coordsInt != nullptr)
	{
		coordsInt[0] = x;
		coordsInt[1] = y;
	}
}