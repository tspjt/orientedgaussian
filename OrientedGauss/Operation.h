#pragma once

#include "NImage.h"
#include <qdebug.h>
#include "Mask.h"
#include "GeneralTypes.h"
#include <cmath>
#include <iostream>
#include <algorithm>
#include <fstream>

double gPM(double s, double KK);

class Operation
{
public:
	virtual void Do(const OperationLHEImplicitParams &params) = 0;

protected:
	Operation();
	void ResizeOutputImageIfNeeded(const NImage& from, NImage& to) const;
	void ResizeOutputImageIfNeeded(const QImage& from, NImage& to) const;
	void ResizeOutputImageIfNeeded(const QImage& from, QImage& to) const;
	double Mean(const NImage& img) const;
	double SumNeighbourValues(int row, int column, const NImage& img) const;
	double L2NormSquared(const NImage& img) const;
};

/***************************************/
//
//
//			LHE_IMPLICIT
//
//
/***************************************/

class OperationLHEImplicit : public Operation
{
public:
	OperationLHEImplicit();
	void Do(const OperationLHEImplicitParams &params);

private:
	NImage m_iterInput;
	NImage m_gaussSeidelVar;
	NImage m_sorIterResult;
	NImage m_iterResult;
	
	double m_tau;
	double m_tol;
	double m_omega;
	double m_ap;
	double m_aq;
	double m_res = std::numeric_limits<double>::max();

	void GaussSeidel();
	void DoIteration();
	void SOR();
	void DoSORIteration();
};