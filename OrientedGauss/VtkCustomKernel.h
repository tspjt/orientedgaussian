#pragma once

#include <vtkImageConvolve.h>
#include <vtkObjectFactory.h>

class VtkCustomKernel : public vtkImageConvolve
{
public:
	void SetCustomKernel(const double* kernel, int x, int y);
	static VtkCustomKernel *New();

	vtkTypeMacro(VtkCustomKernel, vtkImageConvolve);

protected:
	VtkCustomKernel();
	~VtkCustomKernel();

private:
	VtkCustomKernel(const VtkCustomKernel&);
	void operator=(const VtkCustomKernel&);
};

