#pragma once

#include <qwidget.h>
#include <qstring>
#include <string>
#include <vector>
#include "ui_ViewerWidget.h"
#include <QVTKOpenGLNativeWidget.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkImageActor.h>
#include <vtkImageMapper3D.h>
#include <vtkImageReader2.h>
#include <vtkImageReader2Factory.h>
#include <vtkInteractorStyleImage.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <vtkImageViewer2.h>
#include <vtkEllipseArcSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkImageProperty.h>
#include <vtkImageResize.h>
#include <gsl/gsl_rng.h>
#include <vtkImageConvolve.h>
#include "VtkCustomKernel.h"
#include <vtkImageCast.h>
#include <vtkImageMapToColors.h>
#include <vtkImageData.h>
#include <vtkImageExtractComponents.h>
#include <vtkInteractionCallback.h>
#include <vtkCornerAnnotation.h>
#include <vtkTextProperty.h>
#include <vtkExtractVOI.h>
#include "VtkCustomInteractorStyle.h"
#include <vtkOpenGLRenderer.h>
#include <vector>
#include "NImage.h"
#include "Mask.h"
#include <vtkPropCollection.h>
#include <vtkRendererCollection.h>
#include <QMainWindow>
#include "NewImageDialog.h"
#include "GeneralTypes.h"
#include <cmath>
#include <omp.h>
#include "VtkInteractionCallback.h"
#include <vtkImageAppend.h>
#include "Operation.h"
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <chrono>

#define K 0.001

class ViewerWidget : public QWidget
{
	Q_OBJECT

public:
	ViewerWidget(QWidget* parent = Q_NULLPTR, QVTKOpenGLNativeWidget *widget = nullptr, QMainWindow* window = nullptr);
	~ViewerWidget();

	void CleanUpForNextWidget();
	void TakeOverRenderWindow();
	void SetOriginalImage(const QString &fileName);
	void SetOriginalImage(vtkSmartPointer<vtkImageData> imageData);
	inline int GetCurrentTabIndex() const { return ui->tabWidget->currentIndex(); }
	void SelectionOn();
	void EllipseOn();
	void EllipseOff();
	void RemoveEllipses();
	void Gaussian(int numIters);
	void Tensor(int numIters);
	void LHE(const OperationLHEImplicitParams &params) const;
	void LHECustom(const OperationLHEImplicitParams &params, int numIters);
	void SetCParam(double par) { m_cParam = par; }
	void SetLambda(double lambda) { m_lambda = lambda; }
	void SetLHESourceParams(const OperationLHEImplicitParams &params) { m_sourceParams = params; }
	void SetLHEAParams(const OperationLHEImplicitParams &params) { m_aParams = params; }
	void SetLHEBParams(const OperationLHEImplicitParams &params) { m_bParams = params; }
	void SetLHECParams(const OperationLHEImplicitParams &params) { m_cParams = params; }
	void ExportImageToVtk(const std::string &path);
	void ExportImageToPGM(const std::string &path);

private:
	Ui::ViewerWidget* ui;
	NewImageDialog m_newImageDialog;

	double m_cParam = 0.5;
	double m_lambda = 0.2;
	OperationLHEImplicitParams m_sourceParams;
	OperationLHEImplicitParams m_aParams;
	OperationLHEImplicitParams m_bParams;
	OperationLHEImplicitParams m_cParams;

	QVTKOpenGLNativeWidget *m_glWidget = nullptr;
	QMainWindow *m_window = nullptr;
	vtkSmartPointer<vtkCornerAnnotation> m_cornerAnnotations[3];
	vtkSmartPointer<vtkImageViewer2> m_imageViewer;
	vtkSmartPointer<vtkRenderWindowInteractor> m_interactor;
	vtkSmartPointer<VtkCustomInteractorStyle> m_style;
	vtkSmartPointer<VtkInteractionCallback> m_callback;
	vtkSmartPointer<vtkOpenGLRenderer> m_infoRenderers[3];
	std::vector<GaussianData> m_gaussianData;
	int m_lastTabIndex = 0;
	vtkSmartPointer<vtkImageActor> m_originalImage;
	vtkSmartPointer<vtkImageData> m_originalImageData;
	vtkSmartPointer<vtkImageData> m_resultImageData;
	NImage m_originalNImage;
	NImage m_resultImage;

	void ShowResult(vtkSmartPointer<vtkImageData> imageData);

public slots:
	void SwitchTabs(int index);
	void SetEllipseScale(double scale);

private slots:
	void on_tabWidget_currentChanged(int index);


signals:
	void TabChanged(int tabIndex);
};

