#pragma once

#include <qdialog.h>
#include "ui_EllipseScaleDialog.h"

class EllipseScaleDialog : public QDialog
{
	Q_OBJECT

public:
	EllipseScaleDialog(QWidget* parent = Q_NULLPTR);

private:
	Ui::EllipseScaleDialog* ui;

private slots:
	void on_pushButtonOK_pressed();
	void on_pushButtonCancel_pressed();
	void on_horizontalSlider_sliderMoved(int val);
	void on_doubleSpinBox_valueChanged(double val);

signals:
	void Accepted(double);
};